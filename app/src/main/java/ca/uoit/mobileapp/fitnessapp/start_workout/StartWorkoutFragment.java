package ca.uoit.mobileapp.fitnessapp.start_workout;

import android.app.Fragment;
import android.content.Intent;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import java.util.ArrayList;

import ca.uoit.mobileapp.fitnessapp.ExerciseHowToActivity;
import ca.uoit.mobileapp.fitnessapp.R;
import ca.uoit.mobileapp.fitnessapp.models.*;
import static ca.uoit.mobileapp.fitnessapp.WorkoutsActivity.EXTRA_WORKOUT_NAME;

/**************************************************************************************************
 * start_workout -> StartWorkoutFragment.java                                                     *
 * ************************************************************************************************
 *
 * Author(s)                | Content Added
 * -----------------------------------------------------------------------------------------------
 * Sheron Balasingam        | Pulls workout from WorkoutHelper(db), extracts exercises and uses it to
 *                          | create warmup workout sets in createWarmupSets().
 *                          | Added interfaces to communicate with ExercisesFragment (sends exercise
 *                          | list to it) and WarmupsFragment (sends generated warmup exercise list to
 *                          | it).
 *                          | Sets up workout set information initially and after each exercise set
 *                          | is finished by the user. Also calls resetTimer() and hideTimer() in
 *                          | StartWorkoutActivity to display when the user finishes a main workout
 *                          | set.
 *------------------------------------------------------------------------------------------------
 * Mirical Williams-Causton | Added a button to get to the ExerciseHowToActivity and added
 *                          | functionality to it
 *                          | Changed four buttons to image buttons, in order to workout
 *                          | with the changes I made to the layout
 * ----------------------------------------------------------------------------------------------
 * References:
 * ----------------------------------------------------------------------------------------------
 * https://developer.android.com/studio/write/image-asset-studio.html
 * ============================================================================================== *
 * This fragment is displayed part of StartWorkoutActivity.
 *
 * Gets the exercises for the workout from WorkoutHelper(db) and sends it through
 * OnExercisesReceivedListener(interface) to ExercisesFragment. Using the information in the
 * exercises pulled from the db, warmup workout sets are created and sent through
 * OnWarmupsCreatedListener(interface) to WarmupsFragment.
 *
 **************************************************************************************************/
public class StartWorkoutFragment extends Fragment implements View.OnClickListener {

    // Interfaces
    // ============================================================================================
    // Interface implemented in ExercisesFragment
    public OnExercisesReceivedListener onExercisesReceivedListener;

    // exercisesReceived sends the list of exercises
    public interface OnExercisesReceivedListener {
        void exercisesReceived(ArrayList<Exercise> exercises);
    }

    public void setOnExercisesReceivedListener(
            OnExercisesReceivedListener onExercisesReceivedListener) {
        this.onExercisesReceivedListener = onExercisesReceivedListener;
    }

    // Interface implemented in WarmupsFragment
    public OnWarmupsCreatedListener onWarmupsCreatedListener;

    // warmupsCreated sends list of warmups and exercise names
    public interface OnWarmupsCreatedListener {
        void warmupsCreated(SparseArray<ArrayList<Set>> warmups, ArrayList<String> exerciseNames);
    }

    public void setOnWarmupsCreatedListener(OnWarmupsCreatedListener onWarmupsCreatedListener) {
        this.onWarmupsCreatedListener = onWarmupsCreatedListener;
    }
    // ============================================================================================


    // Global Variables
    // ============================================================================================
    StartWorkoutActivity act;
    WorkoutHelper workoutHelper;
    Workout workout;
    String workoutName, exerciseName;

    // Workout set vars
    ArrayList<Exercise>         exercises;
    Exercise                    exercise;
    ArrayList<Set> warmupSets, sets;
    SparseArray<ArrayList<Set>> warmupExercises;
    Set                         set;

    // Workout/exercise var
    int restTime       = 0;
    int exIndex        = 0;
    int setIndex       = 0;
    int numMainSets    = 0;
    int numWarmupSets  = 0;
    int reps           = 0;
    int totalReps      = 0;
    float weightChange = 0.0f;
    float weightMin    = 0.0f;
    float weight       = 0.0f;
    float totalWeight  = 0.0f;
    String equipment   = "";
    boolean doWarmupSet     = true;
    boolean doMainSet       = false;
    boolean finishedWorkout = false;

    // Layout vars
    ViewGroup vgEquipment;
    EquipmentView equipmentView;

    EditText txtReps, txtWeight;
    TextView lblExerciseTitle, lblSetInfo;
    Button btnFinishSet, btnCancelSet, btnFinishWorkout, btnHowTo;
    ImageButton btnDecReps, btnIncReps, btnDecWeight, btnIncWeight;
    // ============================================================================================


    // Required Empty Constructor
    public StartWorkoutFragment() {
    }

    // StartWorkoutFragment
    ///////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        act = (StartWorkoutActivity) getActivity();

        warmupExercises = new SparseArray<>();
        workoutHelper = new WorkoutHelper(act);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_start_workout, container, false);

        txtReps   = (EditText) view.findViewById(R.id.txtReps);
        txtWeight = (EditText) view.findViewById(R.id.txtWeight);

        // Exercise name and Set number
        lblExerciseTitle = (TextView) view.findViewById(R.id.lblExerciseTitle);
        lblSetInfo       = (TextView) view.findViewById(R.id.lblSetInfo);

        btnHowTo = (Button) view.findViewById(R.id.btnHowTo);

        // Exercise weights amount and reps control buttons
        btnDecReps   = (ImageButton) view.findViewById(R.id.btnDecReps);
        btnIncReps   = (ImageButton) view.findViewById(R.id.btnIncReps);
        btnDecWeight = (ImageButton) view.findViewById(R.id.btnDecWeight);
        btnIncWeight = (ImageButton) view.findViewById(R.id.btnIncWeight);

        // Finishing a set/workout control buttons
        btnFinishSet     = (Button) view.findViewById(R.id.btnFinishSet);
        btnCancelSet     = (Button) view.findViewById(R.id.btnCancelSet);
        btnFinishWorkout = (Button) view.findViewById(R.id.btnFinishWorkout);

        // View to insert EquipmentView
        vgEquipment = (ViewGroup) view.findViewById(R.id.rlEquipmentDisplay);

        // Listener to set draw the equipment image when the weight amount is changed
        txtWeight.addTextChangedListener(getTextWatcher());

        lblSetInfo.setTypeface(null, Typeface.BOLD);

        btnHowTo.setOnClickListener(this);

        // Reps and weight change listeners
        btnDecReps.setOnClickListener(this);
        btnIncReps.setOnClickListener(this);
        btnDecWeight.setOnClickListener(this);
        btnIncWeight.setOnClickListener(this);

        // Finish set/workout control listeners
        btnFinishSet.setOnClickListener(this);
        btnCancelSet.setOnClickListener(this);
        btnFinishWorkout.setOnClickListener(this);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        workoutName = getArguments().getString(EXTRA_WORKOUT_NAME);
        workout     = workoutHelper.getWorkout(workoutName);
        exercises   = workout.getExercises();
        exIndex     = exercises.size();

        onExercisesReceivedListener.exercisesReceived(exercises);
        createWarmupSets();

        // Set up the custom view (EquipmentView) to display the equipment. View added dynamically
        // to trigger onDraw method
        equipmentView = new EquipmentView(act);
        equipmentView.setLayoutParams(
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                           ViewGroup.LayoutParams.MATCH_PARENT)
        );
        vgEquipment.addView(equipmentView, 0);

        // Set up the first workout set information
        setupNextSetInfo();
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////


    /* Generates warmup sets based on main exercise sets and stores warmup sets in a sparse array.
     * At the end of the function, it sets up the custom EquipmentView and adds it dynamically to
     * the view group that holds the middle relative view in the fragment_start_workout.
     */
    public void createWarmupSets() {
        ArrayList<String> exerciseNames = new ArrayList<>();
        float weight, currWeight, weightInc, weightMax;
        int reps, repDec, repMin, numSets;

        // Goes through each exercise in the exercise list and determines reps, weights, and number
        // of sets for each exercise warmup
        int j = 1;
        for (Exercise exercise : exercises) {
            exerciseNames.add(exercise.getName());
            weight = exercise.getWeight();

            // If a barbell is used for equipment, then minimum weight is 45lbs (bar weight)
            currWeight = 0f;
            if (exercise.getEquipment().equals(getString(R.string.barbell))) {
                currWeight = 45f;
            }

            // Max weight lifted for the warmups is set to 85% of the exercise weight
            weightMax = weight * 0.85f;
            warmupSets = new ArrayList<>();


            if (weight > currWeight) {
                reps   = 15;
                repMin = Math.min(exercise.getReps() - 2, 1);

                // Gets log base 5 of the weight + the weight/200 to get the number of sets for
                // the warmups (found this to be ideal through trial and error)
                if ((weightMax - currWeight) <= 5f) {
                    numSets = 1;
                } else {
                    numSets = (int) (Math.log(weight) / Math.log(5) + weight / 200);
                }
                // The reps to decrease by as the weight increases for the next warmup set in the
                // current exercise
                repDec    = (reps - repMin) / numSets;
                weightInc = (weightMax - currWeight) / numSets;

                // Adds the sets to the workout, decreasing the reps and increasing the weight with
                // each loop
                for (int i = 1; i < (numSets + 1); i++) {
                    // Current weight is rounded down to the nearest 5
                    warmupSets.add(new Set(i, reps, (float)(5 * (Math.floor(currWeight / 5)))));
                    currWeight = Math.min(currWeight + weightInc, weightMax);
                    reps = Math.max(repMin, reps - repDec);
                }

                // Adds the warmup sets to the sparse array with the same index as the exercise
                warmupExercises.append(j, warmupSets);
            }

            // incremented even when no warmups are made to keep the exercise index
            j++;
        }

        // Send the warmups through the interface to WarmupsFragment
        onWarmupsCreatedListener.warmupsCreated(warmupExercises, exerciseNames);
    }

    /* Sets up workout sets information to display on screen. This function gets the exercise info
     * if the user is currently doing a warmup set (Warmup sets will be displayed before their
     * corresponding main workout set).
     */
    public void setupNextSetInfo() {

        // Only on the start of a warmup set or main exercise set
        if (setIndex == 0) {

            // Rest time initially set to 1 minute. This value is not changed if a user goes from a
            // main workout to a main workout set (no warmups needed for this set)
            restTime = 60000;

            // Setup warmup sets to do
            if (doWarmupSet) {
                // Start from the exercises in the end of the list which is meant to be done first
                exIndex--;
                exercise = exercises.get(exIndex);
                exerciseName = exercise.getName();
                equipment = exercise.getEquipment();

                // Set exercise name at the top
                lblExerciseTitle.setText(exerciseName);
                btnHowTo.setText((getString(R.string.how_to_do) + " " + exerciseName));

                // Get the warmup exercise sets for the current exercise. If there are none, set
                // the doWarmupSet flag to false and doMainSet flag to true. Otherwise, get and
                // store the number of warmup sets
                sets = warmupExercises.get(exIndex + 1);
                if (sets == null) {
                    doWarmupSet = false;
                    doMainSet = true;
                } else {
                    doMainSet = false;
                    numWarmupSets = sets.size();
                }
            }

            // Set up main sets to do
            if (!doWarmupSet) {
                doMainSet = true;
                sets = new ArrayList<>();
                totalReps = 0;
                totalWeight = 0;

                numMainSets = exercise.getSets();
                reps = exercise.getReps();
                weight = exercise.getWeight();

                // Setup rest time based on the number of reps
                setRestTime(reps);

                for (int i = 0; i < numMainSets; i++) {
                    sets.add(new Set(i + 1, reps, weight));
                }
            }
        }

        // Set up current set
        if (doWarmupSet) {
            setWorkoutSet(numWarmupSets, getString(R.string.warmup_set), equipment);
        } else {
            setWorkoutSet(numMainSets, getString(R.string.main_set), equipment);
        }
    }

    /* Sets up the current set information to display on screen. Displays the set, reps, weight.
    */
    public void setWorkoutSet(int maxSets, String setType, String equipment) {
        if (setIndex < maxSets) {
            set    = sets.get(setIndex);
            reps   = set.getReps();
            weight = set.getWeight();

            // Displays warmup set # or main set
            lblSetInfo.setText((setType + " " + (setIndex + 1)));
            txtReps.setText(String.valueOf(reps));
            txtWeight.setText(String.valueOf(weight));

            // TODO: needs to display different equipment (currently only barbell)
            // Displays 2d graphics of equipment
            displayEquipment(weight, equipment);

            // Increment the set index for the exercise and check if the sets are done
            setIndex++;
            if (setIndex == maxSets) {
                doWarmupSet = false;
                if (setType.equals(getString(R.string.main_set))) {
                    if (exIndex == 0) {
                        finishedWorkout = true;
                    } else {
                        doWarmupSet = true;
                    }

                }
                setIndex = 0;
            }
        }
    }

    /*
     * Calls EquipmentView's setup method, which sets up the objects to draw and invalidates the
     * view to trigger the onDraw method.
     */
    public void displayEquipment(float weight, @Nullable String equipment) {
        if (equipment != null) {
            if (equipment.equals(getString(R.string.barbell))) {
                weightChange = 10.0f;
                weightMin = 45.0f;
            } else {
                weightChange = 5.0f;
                weightMin = 0.0f;
            }
        }

        equipmentView.setup(weight, equipment);
    }

    /* Sets the rest time based on the number of reps.
     */
    private void setRestTime(int reps) {
        if (reps <= 3) {
            restTime = 240000;
        } else if (reps <= 5) {
            restTime = 180000;
        } else if (reps <= 8) {
            restTime = 60000;
        } else {
            restTime = 45000;
        }
    }
    // ============================================================================================


    // Listeners
    // ============================================================================================
    // Returns a TextWatcher object. Used with the weight text field.
    public TextWatcher getTextWatcher() {
        return new TextWatcher() {
            String txt = "";
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            // After the text is changed, displayEquipment is called to change the equipment
            // display
            @Override
            public void afterTextChanged(Editable s) {
                txt = txtWeight.getText().toString();
                if (!txt.isEmpty()) {
                    displayEquipment(Float.valueOf(txtWeight.getText().toString()), null);
                }
            }
        };
    }
    // ============================================================================================


    // Click handling
    // ============================================================================================
    public void handleClick(int id) {

        switch (id) {
            // Makes sure reps text field value is >= 0
            case R.id.btnDecReps:
                reps = Math.max(0, Integer.valueOf(txtReps.getText().toString()) - 1);
                txtReps.setText(String.valueOf(reps));
                break;

            case R.id.btnIncReps:
                reps++;
                txtReps.setText(String.valueOf(reps));
                break;

            // Makes sure weight text field value is >= 0 or 45 depending on the equipment used
            case R.id.btnDecWeight:
                weight = Math.max(weightMin,
                        Float.valueOf(txtWeight.getText().toString()) - weightChange);
                displayEquipment(weight, null);
                txtWeight.setText(String.valueOf(weight));
                break;

            case R.id.btnIncWeight:
                weight += weightChange;
                displayEquipment(weight, null);
                txtWeight.setText(String.valueOf(weight));
                break;

            case R.id.btnFinishSet:
                handleFinishedSet();
                break;

            case R.id.btnHowTo:
                Intent getHowToExerciseActivity = new Intent(act, ExerciseHowToActivity.class);
                getHowToExerciseActivity.putExtra("Exercise Name", exerciseName);
                startActivity(getHowToExerciseActivity);
                break;

            // TODO: change to handle recording 0 reps and change value in ExerciseFragment if main set
            case R.id.btnCancelSet:
                act.finish();
                break;

            case R.id.btnFinishWorkout:
                act.finish();
                break;
        }
    }

    /* Handles a finished workout set by either resetting the timer (if next set is not a warmup)
     * or hides the timer (if the next set is a warmup).
     * If the user also completed a set with a new weight, different from the initial weight, the
     * new set will initially be stay at that weight (iff the same number of reps or greater were
     * done).
     * Next the weight and reps done for an exercise are averaged over its sets and compared with
     * weights and reps that were initially set. If these averages are greater than equal to the
     * initially set attributes, then the weight plus and increment is added to WorkoutHelper(db)
     * so this increment of weight will be used the next time the user does this workout.
     */
    private void handleFinishedSet() {
        if (!doWarmupSet) {
            act.resetTimer(restTime);
        } else {
            act.hideTimer();
        }

        if (doMainSet) {
            Float currWeight = Float.valueOf(txtWeight.getText().toString());
            int currReps = Integer.valueOf(txtReps.getText().toString());

            totalWeight += currWeight;
            totalReps += currReps;

            // If a greater weight was performed for the same reps or more, set the weight value
            // for the next set to this value
            if (currReps >= set.getReps()) {
                if (currWeight >= set.getWeight()) {
                    if (setIndex < numMainSets) {
                        sets.get(setIndex).setWeight(currWeight);
                    }
                }
            } else {
                if (setIndex < numMainSets) {
                    sets.get(setIndex).setWeight(Math.max(currWeight - weightChange, weightMin));
                }
            }

            // Update weight value in WorkoutHelper(db)
            if (doWarmupSet || finishedWorkout) {
                doMainSet = false;
                currWeight = totalWeight / (float)numMainSets;
                currReps = totalReps / numMainSets;
                float setWeight = set.getWeight();

                // If user does same number of reps or more and the same weight or more on average
                // for the exercise sets, then updated db value with an increment
                if (currWeight >= setWeight && currReps >= set.getReps()) {

                    currWeight = (float)(5 * Math.floor(currWeight / 5));
                    float maxWeight = (float)((int)Math.floor(Math.max(currWeight,
                                                                       setWeight) * 10)) / 10.0f;

                    workoutHelper.updateWeight(workoutName, exerciseName, weightChange + maxWeight);

                // If user does less weight or less reps, the decrease weight value in db
                } else {
                    workoutHelper.updateWeight(
                            workoutName,
                            exerciseName,
                            Math.max(currWeight - weightChange, weightMin)
                    );
                }

            }
        }

        // Setup next info if not at the end of the workout
        if (!finishedWorkout) {
            setupNextSetInfo();

        // If at the end of the workout, display Finish Workout btn and hide other btns
        } else {
            btnFinishSet.setVisibility(View.GONE);
            btnCancelSet.setVisibility(View.GONE);
            btnHowTo.setVisibility(View.GONE);
            btnFinishWorkout.setVisibility(View.VISIBLE);
            lblExerciseTitle.setText(R.string.workout_complete);
            lblSetInfo.setText("");
        }
    }

    @Override
    public void onClick(View v) {
        handleClick(v.getId());
    }
    // ============================================================================================
}
