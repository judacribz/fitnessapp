package ca.uoit.mobileapp.fitnessapp;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;

import ca.uoit.mobileapp.fitnessapp.adapters.ButtonAdapter;
import ca.uoit.mobileapp.fitnessapp.adapters.ButtonItemClickListener;
import ca.uoit.mobileapp.fitnessapp.adapters.SpinnerAdapter;
import ca.uoit.mobileapp.fitnessapp.start_workout.StartWorkoutActivity;
import ca.uoit.mobileapp.fitnessapp.models.User;
import ca.uoit.mobileapp.fitnessapp.models.WorkoutHelper;

import static ca.uoit.mobileapp.fitnessapp.utilities.HelperFunctions.setToolBar;

/**************************************************************************************************
 * WorkoutsActivity.java                                                                          *
 * ************************************************************************************************
 *
 * Author(s)                | Content Added
 * ------------------------------------------------------------------------------------------------
 * Sheron Balasingam        | Added recyclerView display of all user's workouts in button list format
 *                          | Added a search bar which filters out the workout buttons as user types
 *                          | Custom recycler view click handling (ButtonItemClickListener)
 *                          |
 *------------------------------------------------------------------------------------------------
 * Mirical Williams-Causton | Added menu item for add workout activity and added functionality to it
 *                          | Added menu item for delete workout activity and add functionality to it
 *                          | Created a function to delete workouts from the local database
 *                          | Created two methods for the dialog delete and cancel buttons
 *-------------------------------------------------------------------------------------------------
 *                          |
 * ------------------------------------------------------------------------------------------------
 * Resource(s)
 * ------------------------------------------------------------------------------------------------
 * https://www.codeproject.com/tips/659766/android-custom-dialogbox
 * https://developer.android.com/guide/topics/ui/dialogs.html
 * ================================================================================================
 * This fragment is displayed part of StartWorkoutActivity.
 **************************************************************************************************/
public class WorkoutsActivity extends AppCompatActivity
        implements ButtonItemClickListener.OnItemClickListener {

    // Constants
    // ============================================================================================
    public static final String EXTRA_WORKOUT_NAME =
            "ca.uoit.mobileapp.fitnessapp.EXTRA_WORKOUT_NAME";
    private static final int ADD_WORKOUT_REQUEST = 101;
    // ============================================================================================

    // Global Variables
    // ============================================================================================
    RecyclerView workoutsList;
    ButtonAdapter workoutAdapter;
    LinearLayoutManager layoutManager;
    EditText searchBar;

    public Spinner workoutSpinner;
    public Button deleteWorkout;
    public Dialog dialog;

    ArrayList<String> workoutNames;
    ArrayList<String> filteredWorkouts;
    WorkoutHelper workoutHelper;
    // ============================================================================================


    // WorkoutsActivity onCreate
    ///////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workouts);
        setToolBar(WorkoutsActivity.this, R.string.workouts, true);

        String email = User.getInstance().getEmail();
        workoutNames = new ArrayList<>();

        // Get recycler view reference
        workoutsList = (RecyclerView) findViewById(R.id.rvButtons);
        searchBar    = (EditText) findViewById(R.id.txtSearch);

        // Set the layout manager for the localWorkouts
        layoutManager = new LinearLayoutManager(this);
        workoutsList.setLayoutManager(layoutManager);
        workoutsList.setHasFixedSize(true);

        // Get all workouts from database
        workoutHelper = new WorkoutHelper(this);
        workoutNames = workoutHelper.getAllWorkoutNames();
        displayBtnList(workoutNames);
    }

    @Override
    protected void onStop() {
        super.onStop();
        searchBar.setText("");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case ADD_WORKOUT_REQUEST:
                    workoutNames = filteredWorkouts = workoutHelper.getAllWorkoutNames();
                    workoutAdapter = new ButtonAdapter(filteredWorkouts);
                    workoutsList.setAdapter(workoutAdapter);
                    break;
            }
        }
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////


    // Displays button list of workouts
    void displayBtnList(ArrayList<String> workouts) {
        filteredWorkouts = workouts;
        workoutAdapter = new ButtonAdapter(workouts);
        workoutsList.setAdapter(workoutAdapter);

        // Set the button click listener for the recycler view
        workoutsList.addOnItemTouchListener(new ButtonItemClickListener(this,
                                                                        workoutsList,
                                                                        this));
        // Add the text watcher to the search bar
        searchBar.addTextChangedListener(getTextWatcher());
    }


    // Used to filter RecyclerView list using based on the text entered in the search bar
    public TextWatcher getTextWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                filteredWorkouts = new ArrayList<>();
                String filter = (searchBar.getText().toString().trim()).toLowerCase();

                for (String workoutName : workoutNames) {
                    if (workoutName.toLowerCase().contains(filter)) {
                        filteredWorkouts.add(workoutName);
                    }
                }

                if (filteredWorkouts != null) {
                    workoutsList.setAdapter(new ButtonAdapter(filteredWorkouts));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
    }


    // Click handling
    // ============================================================================================
    public void handleClick(int id) {
        switch (id) {
            // Add workout menu item click handling
            case R.id.actAddWorkout:
                startAddWorkout();
                break;
            //Delete workout menu item click handling
            case R.id.actDeleteWorkout:
                startDeleteWorkout();
                break;
            // Toolbar back btn
            case android.R.id.home:
                finish();
                break;
        }

    }

    // Starts AddWorkoutActivity
    public void startAddWorkout() {
        Intent addWorkout = new Intent(this, AddWorkoutActivity.class);
        startActivityForResult(addWorkout, ADD_WORKOUT_REQUEST);
    }

// ------------------------------------------------------------------------------------------------
    /* Start Delete Workout Dialog Box
     * Shows a dialog box on top the workouts activity
     * When the use selects a workout from the spinner and clicks the delete button
     * the workout will be deleted from the local database for that user signed in, once
     * deleted the dialog button dismisses.
     * If the cancel button is clicked the dialog is dismissed.
     * */
    public void startDeleteWorkout() {
        dialog = new Dialog(WorkoutsActivity.this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.delete_workout);

        workoutSpinner = dialog.findViewById(R.id.sprDeleteWorkout);
        deleteWorkout = dialog.findViewById(R.id.btnDeleteWorkout);

        workoutSpinner.setAdapter(new SpinnerAdapter(this, workoutNames));

        dialog.show();
    }

    public void getDelete(View view) {
        String workoutName = workoutSpinner.getSelectedItem().toString();
        workoutHelper.deleteWorkout(workoutName);

        workoutNames = filteredWorkouts = workoutHelper.getAllWorkoutNames();
        workoutAdapter = new ButtonAdapter(filteredWorkouts);
        workoutsList.setAdapter(workoutAdapter);

        dialog.dismiss();
    }

    public void getCancel(View view) {
        dialog.dismiss();
    }
// ------------------------------------------------------------------------------------------------

    // Starts StartWorkoutActivity
    public void startWorkout(String workoutName) {
        Intent workout = new Intent(this, StartWorkoutActivity.class);
        workout.putExtra(EXTRA_WORKOUT_NAME, workoutName);
        startActivity(workout);
    }

    // Button and Menu Click Overrides
    // --------------------------------------------------------------------------------------------
    // Interface: wrapper that handles recyclerView list of workouts onClick
    @Override
    public void onItemClick(View view, int position) {
        startWorkout(filteredWorkouts.get(position));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_workouts_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        handleClick(item.getItemId());
        return super.onOptionsItemSelected(item);
    }

    // ============================================================================================
}
