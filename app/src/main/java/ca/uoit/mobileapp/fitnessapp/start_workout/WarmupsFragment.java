package ca.uoit.mobileapp.fitnessapp.start_workout;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import ca.uoit.mobileapp.fitnessapp.R;
import ca.uoit.mobileapp.fitnessapp.models.Set;

/**************************************************************************************************
 * start_workout -> WarmupsFragment.java                                                          *
 * ************************************************************************************************
 *
 * Author(s)            | Content Added
 * ------------------------------------------------------------------------------------------------
 * Sheron Balasingam    | Displays workout warmup sets (set #, reps, weight) for each exercise in
 *                      | a recyclerView list(added in dynamically because of varying amounts of
 *                      | warmup sets can be created depending on the weight for the main sets).
 *-------------------------------------------------------------------------------------------------
 *                      |
 * ================================================================================================
 * This fragment is displayed part of StartWorkoutActivity.
 *
 * Displays all the sets of warmup exercise sets for the current workout.
 *
 **************************************************************************************************/
public class WarmupsFragment extends Fragment
        implements StartWorkoutFragment.OnWarmupsCreatedListener {

    // Global Variables
    // ============================================================================================
    StartWorkoutActivity act;

    ViewGroup vgSets, vgSubtitle;
    LayoutInflater layInflater;
    // ============================================================================================


    // Required Empty Constructor
    public WarmupsFragment() {
    }

    // StartWorkoutFragment @Override
    ///////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        act = (StartWorkoutActivity) getActivity();

        act.START_WORKOUT_FMT.setOnWarmupsCreatedListener(this);
        layInflater = act.getLayoutInflater();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_warmups, container, false);

        vgSubtitle = (ViewGroup) view.findViewById(R.id.llWarmupSubtitleInsert);
        vgSets = (ViewGroup) view.findViewById(R.id.llWarmupSetsInsert);

        return view;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////


    // Interface implementations
    // ============================================================================================
    // Receives the warmup sets from StartWorkoutFragment and sets them up to be displayed in
    // recyclerView lists
    @Override
    public void warmupsCreated(SparseArray<ArrayList<Set>> warmups,
                               ArrayList<String> exerciseNames) {
        int key;
        for (int i = 0; i < warmups.size(); i++) {
            key = warmups.keyAt(i);
            act.displaySets(200 + key,
                            exerciseNames.get(key-1),
                            warmups.valueAt(i),
                            vgSubtitle,
                            vgSets);
        }
    }
    // ============================================================================================
}