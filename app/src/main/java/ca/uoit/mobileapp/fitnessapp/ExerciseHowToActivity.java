package ca.uoit.mobileapp.fitnessapp;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

/**************************************************************************************************
 * ExerciseHowToActivity.java                                                                             *
 * ************************************************************************************************
 *
 * Author(s)                | Content Added
 * ------------------------------------------------------------------------------------------------
 * Mirical Williams-Causton | Added functionality to the youtube player
 *                          | Added a way to manually change the toolbar title
 *                          | Cued youtube videos based on the exercise being completely in the
 *                          | startWorkoutFragment
 * ------------------------------------------------------------------------------------------------
 *                          |
 *
 * Resource(s)
 * ------------------------------------------------------------------------------------------------
 * https://www.sitepoint.com/using-the-youtube-api-to-embed-video-in-an-android-app/
 * https://www.youtube.com/watch?v=t2b8UdqmlFs
 * https://www.youtube.com/watch?v=esQi683XR44
 * https://www.youtube.com/watch?v=d5eGGZXb0Is
 * http://stacktips.com/tutorials/android/youtube-android-player-api-example
 * https://stackoverflow.com/questions/13731192/youtube-intent-error-400/13732170
 * https://github.com/codepath/android_guides/wiki/Streaming-Youtube-Videos-with-YouTubePlayerView
 * https://www.youtube.com/watch?v=pyBHPoC8Nzo
 * https://www.youtube.com/watch?v=uO_CNYidOw0
 * https://www.youtube.com/watch?v=0hiReOsKNSQ
 * https://www.youtube.com/watch?v=ZO81bExngMI
 * https://stackoverflow.com/questions/12128331/how-to-change-fontfamily-of-textview-in-android
 * ================================================================================================
 * Plays how to youtube videos depending on the exercise being done in the startWorkoutFragment
 *
 **************************************************************************************************/

public class ExerciseHowToActivity extends YouTubeBaseActivity
        implements View.OnClickListener, YouTubePlayer.OnInitializedListener {

    public Intent getWorkoutNameIntent;
    private static final String api = "AIzaSyBNJjiYt0IwoFyNDOA7GsNZ8gCa6RvMo24";
    public String exerciseName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_how_to);

        TextView toolbarTitle = (TextView)findViewById(R.id.lblToolbarTitle);

        toolbarTitle.setTypeface(Typeface.createFromAsset(getAssets(), "lobster.otf"));
        toolbarTitle.setText(setTitle());

        ((Button)findViewById(R.id.btnClose)).setOnClickListener(this);

        YouTubePlayerView youTubePlayerView = (YouTubePlayerView)findViewById(R.id.youtube_player);
        youTubePlayerView.initialize(api, this);
    }

    public String setTitle() {
        getWorkoutNameIntent = getIntent();
        exerciseName = getWorkoutNameIntent.getStringExtra("Exercise Name");
        return "How to Do " + exerciseName;
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider,
                                        YouTubeInitializationResult result) {
        Toast.makeText(this, "Error trying to load video", Toast.LENGTH_SHORT).show();
    }

    /* Youtube Player, plays youtube how to videos based on the exercise name */
    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                        YouTubePlayer youTubePlayer, boolean wasRestored) {
        /* Hardcoded for now */
        if (!wasRestored) {
            switch (exerciseName) {
                case "Squats": youTubePlayer.cueVideo("t2b8UdqmlFs"); break;
                case "Bench Press": youTubePlayer.cueVideo("esQi683XR44"); break;
                case "Deadlifts": youTubePlayer.cueVideo("d5eGGZXb0Is"); break;
                case "Barbell Row": youTubePlayer.cueVideo("pyBHPoC8Nzo");
                case "Tricep Kickback": youTubePlayer.cueVideo("ZO81bExngMI"); break;
                case "Lat Pulldown": youTubePlayer.cueVideo("0hiReOsKNSQ"); break;
                case "Bicep Curls": youTubePlayer.cueVideo("uO_CNYidOw0"); break;
            }
        }
    }

    /* Closes activity when close button is pressed */
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnClose: {
                finish();
            }
        }
    }
}
