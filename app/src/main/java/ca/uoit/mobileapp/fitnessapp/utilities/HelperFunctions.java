package ca.uoit.mobileapp.fitnessapp.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;

import ca.uoit.mobileapp.fitnessapp.R;

/**************************************************************************************************
 *  utilities -> Firebase.java                                                                    *
 * ************************************************************************************************
 *
 * Author(s)            | Content Added
 * ------------------------------------------------------------------------------------------------
 * Sheron Balasingam    | Function (handleBackButton) to handle the back button pressed in
 *                      | MainActivity, and LoginActivity (exit app when pressed twice)
 *                      | Two setToolBar functions to set the toolbar in the activity calling the
 *                      | function. One function has res id input while the other has string input
 *                      | for the toolbar title.
 *                      | Function (dbExists) used to check if the WorkoutHelper db exists.
 *-------------------------------------------------------------------------------------------------
 *                      |
 *
 * Resource(s)
 * ------------------------------------------------------------------------------------------------
 * https://firebase.google.com/docs/database/android/read-and-write
 *
 * ================================================================================================
 * Helper functions used throughout the program that are not related to Firebase.
 *
 **************************************************************************************************/
public class HelperFunctions {

    private static boolean backPressedTwice = false;

    // Exits app and goes to home screen if back pressed twice from this screen
    public static void handleBackButton(Context context) {
        if (backPressedTwice) {
            Intent exitApp = new Intent(Intent.ACTION_MAIN);
            exitApp.addCategory(Intent.CATEGORY_HOME);
            exitApp.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(exitApp);
        } else {
            backPressedTwice = true;
            Toast.makeText(context, "Press back again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    backPressedTwice = false;
                }
            }, 2000);
        }
    }

    // Sets the title toolbar using a string id
    public static String setToolBar(Activity activity, int titleId, boolean setBackArrow) {
        return setToolBar(activity, activity.getResources().getString(titleId), setBackArrow);
    }

    // Sets the title toolbar for the Activity
    public static String setToolBar(Activity activity, String title, boolean setBackArrow) {
        // Set the toolbar to the activity
        ((AppCompatActivity) activity).setSupportActionBar(
                (Toolbar) activity.findViewById(R.id.toolbar));

        if (setBackArrow) {
            ActionBar actionBar = ((AppCompatActivity) activity).getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setDisplayShowHomeEnabled(true);
            }
        }
        // Set the title for the toolbar
        ((TextView) activity.findViewById(R.id.lblToolbarTitle)).setText(title);

        return title;
    }

    // Checks to see if the database exists
    public static boolean dbExists(Context context) {
        File dbFile = new File(context.getDatabasePath(
                context.getResources().getString(
                        R.string.workouts
                ).toLowerCase()
        ).toString());

        return dbFile.exists();
    }
}
