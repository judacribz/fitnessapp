package ca.uoit.mobileapp.fitnessapp.models;

/**************************************************************************************************
 * models -> User.java                                                                            *
 * ************************************************************************************************
 *
 * Author(s)            | Content Added
 * ------------------------------------------------------------------------------------------------
 * Sheron Balasingam    | Constructors/getters/setters.
 *-------------------------------------------------------------------------------------------------
 *                      |
 *
 * ================================================================================================
 * Singleton class used to store user information for easy access throughout the app session.
 *
 * Set object with the following attributes:
 *      email: user's email used to login to the app.
 *      uid:   user's unique identifier used in firebase
 *
 **************************************************************************************************/
public class User {

    private String email;
    private String uid;
    private static User instance;

    // Constructor
    private User() {
    }

    // Returns the instance if one exists, otherwise creates one and returns it
    public static User getInstance(){
        if (instance == null) {
            instance = new User();
        }

        return instance;
    }

    // Getters and setters
    // ============================================================================================
    public void setEmail(String email) {
        this.email = email;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getEmail() {
        return email;
    }

    public String getUid() {
        return uid;
    }
    // ============================================================================================
}
