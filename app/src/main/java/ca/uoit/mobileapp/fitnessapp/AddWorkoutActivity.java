package ca.uoit.mobileapp.fitnessapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ca.uoit.mobileapp.fitnessapp.adapters.SpinnerAdapter;
import ca.uoit.mobileapp.fitnessapp.models.Exercise;
import ca.uoit.mobileapp.fitnessapp.models.Workout;
import ca.uoit.mobileapp.fitnessapp.models.WorkoutHelper;

import static ca.uoit.mobileapp.fitnessapp.utilities.HelperFunctions.setToolBar;

/**************************************************************************************************
 * LoginActivity.java                                                                             *
 * ************************************************************************************************
 *
 * Author(s)                | Content Added
 * ------------------------------------------------------------------------------------------------
 * Mirical Williams-Causton | Adding spinners dynamically when user clicks on add more exercises label
 *                          | Get exercises from database and populate spinners
 *                          | Retrieve exercises selected from spinners and add the workout to database
 *                          | Handle click when back button is selected
 * ------------------------------------------------------------------------------------------------
 *                          |
 *
 * Resource(s)
 * ------------------------------------------------------------------------------------------------
 * https://stackoverflow.com/questions/4203506/how-can-i-add-a-textview-to-a-linearlayout-dynamically-in-android
 *
 * ================================================================================================
 * Adds workouts to the local database to specific user signed in
 *
 **************************************************************************************************/

public class AddWorkoutActivity extends AppCompatActivity implements View.OnClickListener {

    public Spinner exerciseSpinner, exerciseSpinner1,
                   exerciseSpinner2, exerciseSpinner3,
                   exerciseSpinner4, exerciseSpinner5;
    public EditText workoutName;
    public LinearLayout linearLayout;
    public List<String> exerciseNameList;
    public ArrayList<Exercise> exercises, exerciseArrayList;
    public WorkoutHelper workoutHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_workout);

        ((TextView)findViewById(R.id.lblAddMoreExercises)).setOnClickListener(this);
        ((Button)findViewById(R.id.btnAddWorkout)).setOnClickListener(this);
        workoutName = findViewById(R.id.txtWorkoutName);

        setToolBar(AddWorkoutActivity.this, getString(R.string.add_workout), true);

        getExerciseNames();
        getExercises();
    }

    public void getExercises() {
        exercises = new ArrayList<>();
        exercises = workoutHelper.getAllExercises();
    }

    // Populates the exercises in the spinners
    public void getExerciseNames() {
        exerciseNameList = new ArrayList<>();
        workoutHelper = new WorkoutHelper(this);
        exerciseNameList = workoutHelper.getAllExerciseNames();

        exerciseSpinner1 = findViewById(R.id.sprWorkout1);
        exerciseSpinner2 = findViewById(R.id.sprWorkout2);
        exerciseSpinner3 = findViewById(R.id.sprWorkout3);
        exerciseSpinner4 = findViewById(R.id.sprWorkout4);
        exerciseSpinner5 = findViewById(R.id.sprWorkout5);

        exerciseSpinner1.setAdapter(new ca.uoit.mobileapp.fitnessapp.adapters.SpinnerAdapter(this, exerciseNameList));
        exerciseSpinner2.setAdapter(new ca.uoit.mobileapp.fitnessapp.adapters.SpinnerAdapter(this, exerciseNameList));
        exerciseSpinner3.setAdapter(new ca.uoit.mobileapp.fitnessapp.adapters.SpinnerAdapter(this, exerciseNameList));
        exerciseSpinner4.setAdapter(new ca.uoit.mobileapp.fitnessapp.adapters.SpinnerAdapter(this, exerciseNameList));
        exerciseSpinner5.setAdapter(new ca.uoit.mobileapp.fitnessapp.adapters.SpinnerAdapter(this, exerciseNameList));
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lblAddMoreExercises: { // Adds more spinners to the view
                linearLayout = (LinearLayout)findViewById(R.id.exercises_layout);

                exerciseSpinner = new Spinner(this);
                exerciseSpinner.setId(R.id.sprWorkout); // Setting ID Dynamically
                exerciseSpinner.setLayoutParams(new Spinner.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
                exerciseSpinner.setAdapter(new SpinnerAdapter(this, exerciseNameList));

                linearLayout.addView(exerciseSpinner);

                break;
            }case R.id.btnAddWorkout: {
                exerciseArrayList = new ArrayList<>();

                int firstPosition = exerciseSpinner1.getSelectedItemPosition();
                int secondPosition = exerciseSpinner2.getSelectedItemPosition();
                int thirdPosition = exerciseSpinner3.getSelectedItemPosition();
                int fourthPosition = exerciseSpinner4.getSelectedItemPosition();
                int fifthPosition = exerciseSpinner5.getSelectedItemPosition();

                exerciseArrayList.add(new Exercise(
                        exercises.get(firstPosition).getName(),
                        exercises.get(firstPosition).getType(), exercises.get(firstPosition).getEquipment(),
                        exercises.get(firstPosition).getReps(), exercises.get(firstPosition).getSets(),
                        exercises.get(firstPosition).getWeight()));

                exerciseArrayList.add(new Exercise(
                        exercises.get(secondPosition).getName(),
                        exercises.get(secondPosition).getType(), exercises.get(secondPosition).getEquipment(),
                        exercises.get(secondPosition).getReps(), exercises.get(secondPosition).getSets(),
                        exercises.get(secondPosition).getWeight()));

                exerciseArrayList.add(new Exercise(
                        exercises.get(thirdPosition).getName(),
                        exercises.get(thirdPosition).getType(), exercises.get(thirdPosition).getEquipment(),
                        exercises.get(thirdPosition).getReps(), exercises.get(thirdPosition).getSets(),
                        exercises.get(thirdPosition).getWeight()));

                exerciseArrayList.add(new Exercise(
                        exercises.get(fourthPosition).getName(),
                        exercises.get(fourthPosition).getType(), exercises.get(fourthPosition).getEquipment(),
                        exercises.get(fourthPosition).getReps(), exercises.get(fourthPosition).getSets(),
                        exercises.get(fourthPosition).getWeight()));

                exerciseArrayList.add(new Exercise(
                        exercises.get(fifthPosition).getName(),
                        exercises.get(fifthPosition).getType(), exercises.get(fifthPosition).getEquipment(),
                        exercises.get(fifthPosition).getReps(), exercises.get(fifthPosition).getSets(),
                        exercises.get(fifthPosition).getWeight()));

                if (exerciseSpinner != null) {
                    int extraPosition = exerciseSpinner.getSelectedItemPosition();
                    exerciseArrayList.add(new Exercise(
                            exercises.get(extraPosition).getName(),
                            exercises.get(extraPosition).getType(), exercises.get(extraPosition).getEquipment(),
                            exercises.get(extraPosition).getReps(), exercises.get(extraPosition).getSets(),
                            exercises.get(extraPosition).getWeight()));
                }

                workoutHelper.addWorkout(new Workout(workoutName.getText().toString(), exerciseArrayList));

                setResult(RESULT_OK);
                finish();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        handleClick(item.getItemId());
        return super.onOptionsItemSelected(item);
    }

    public void handleClick(int id) {
        switch (id) {
            case android.R.id.home: finish(); break;
        }
    }
}
