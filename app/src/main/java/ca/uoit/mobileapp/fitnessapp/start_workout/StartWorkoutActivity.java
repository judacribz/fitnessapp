package ca.uoit.mobileapp.fitnessapp.start_workout;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Fade;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.sql.Blob;
import java.util.ArrayList;
import java.util.Locale;

import ca.uoit.mobileapp.fitnessapp.R;
import ca.uoit.mobileapp.fitnessapp.adapters.SetsAdapter;
import ca.uoit.mobileapp.fitnessapp.models.Set;

import static ca.uoit.mobileapp.fitnessapp.WorkoutsActivity.EXTRA_WORKOUT_NAME;
import static ca.uoit.mobileapp.fitnessapp.utilities.HelperFunctions.*;

/**************************************************************************************************
 * start_workout -> StartWorkoutActivity.java                                                     *
 * ************************************************************************************************
 *
 * Author(s)            | Content Added
 * ------------------------------------------------------------------------------------------------
 * Sheron Balasingam    | Three fragments and their functionality.
 *                      | Workout timer and animation for rest time between sets.
 *                      | Function that dynamically adds horizontal recyclerView lists to the
 *                      | fragments that call it.
 *-------------------------------------------------------------------------------------------------
 *                      |
 *
 * ================================================================================================
 * This activity displays three fragments with a bottom navigation bar holding action items for
 * each of the three fragments.
 *
 * The fragments are:
 *      ExerciseFragment:
 *          Displays all exercise sets in the workout in horizontal listViews for each exercise in
 *          the selected workout.
 *
 *      WarmupsFragment:
 *          Displays a generated list of exercise sets based on the main exercise sets.
 *
 *      StartWorkoutFragment:
 *          Pulls the workout information from WorkoutHelper(db) for the selected workout.
 *          Extracts the exercises and sends it through an interface to ExercisesFragment to
 *          display.
 *          Generates warmup exercise sets based on the exercises and sends it through an interface
 *          to WarmupsFragment.
 *
 * This activity also holds a pop up RelativeLayout that acts similar to a snackbar but is
 * displayed with a timer and a label button to pause or resume the timer. The timer is used to
 * show the user how much rest time is remaining after they have completed a workout set. The timer
 * is not used when the user is doing warmup sets.
 *
 **************************************************************************************************/
public class StartWorkoutActivity extends AppCompatActivity
        implements Animation.AnimationListener,
                   BottomNavigationView.OnNavigationItemSelectedListener,
                   View.OnClickListener {

    // Constants
    // ============================================================================================
    // Fragment container id
    private static final int FRAGMENT_CONTAINER_ID = R.id.flWorkoutFragment;

    // Fragment instance vars
    public final WarmupsFragment      WARMUPS_FMT       = new WarmupsFragment();
    public final ExercisesFragment    EXERCISES_FMT     = new ExercisesFragment();
    public final StartWorkoutFragment START_WORKOUT_FMT = new StartWorkoutFragment();

    final Fragment[] FMTS = new Fragment[] {WARMUPS_FMT, EXERCISES_FMT, START_WORKOUT_FMT};
    // ============================================================================================


    // Global Variables
    // ============================================================================================
    // Navigation bar
    BottomNavigationView navigationView;

    // Fragments exercise set lists display vars
    LayoutInflater layInflater;
    View setsView;
    TextView lblExerciseName;
    RecyclerView setList;

    // Fragment vars
    Fragment            fmt;
    FragmentManager     fmtMgr;
    FragmentTransaction fmtTxn;
    String              fmtTag;
    int fmtItemId = R.id.actStartWorkout;

    // Timer vars
    RelativeLayout timerSnack;
    CountDownTimer countDownTimer;
    Animation slide_up, slide_down;
    TextView lblTimerCtrl, lblTimer;
    long currTime = 0;

    String workoutName;
    Bundle args;
    // ============================================================================================


    // StartWorkoutActivity @Override
    ///////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_workout);
        workoutName = setToolBar(StartWorkoutActivity.this,
                                 getIntent().getStringExtra(EXTRA_WORKOUT_NAME),
                                 false);
        setTheme(R.style.WorkoutTheme);

        // Timer 'custom snackbar' setup
        timerSnack   = (RelativeLayout) findViewById(R.id.rlTimer);
        lblTimer     = (TextView) findViewById(R.id.lblTimer);
        lblTimerCtrl = (TextView) findViewById(R.id.lblTimerCtrl);

        // Custom snackbar animation setup
        slide_up   = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        slide_down = AnimationUtils.loadAnimation(this, R.anim.slide_down);

        // Navbar setup
        navigationView = (BottomNavigationView) findViewById(R.id.navWorkout);

        // Setup the layout inflater for adding the recyclerViews dynamically
        layInflater = getLayoutInflater();

        // Setup fragments
        addFragments();

        // Set listeners
        lblTimerCtrl.setOnClickListener(this);
        slide_up.setAnimationListener(this);
        slide_down.setAnimationListener(this);
        navigationView.setOnNavigationItemSelectedListener(this);
        navigationView.setSelectedItemId(R.id.actExercises);
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////


    // Fragment and fragment content functions
    // ============================================================================================
    /* Adds default fragments to the fragment container in this activity
    */
    void addFragments() {

        // Fragment Manager init
        fmtMgr = getFragmentManager();

        // Sends the workout name to all the fragments in Bundle args
        args = new Bundle();
        args.putString(EXTRA_WORKOUT_NAME, workoutName);


        // Adds a fragment one by one and hides a previous fragment if it was in view
        for (Fragment frag : FMTS) {
            frag.setArguments(args);

            fmtTxn = fmtMgr.beginTransaction();

            // Hide fragment if a previous one exists
            if (fmt != null) {
                fmtTxn.hide(fmt);
            }

            // Add fragment
            fmtTxn.add(FRAGMENT_CONTAINER_ID, frag);

            fmtTxn.commit();
            fmt = frag;
        }
    }


    /* Creates horizontal recycler view lists of  set#, reps, weights for each exercise and adds
     * dynamically to the view.
     * Called in ExercisesFragment and WarmupsFragment
     */
    @SuppressLint("InflateParams")
    public void displaySets(int id,
                            String exerciseName,
                            ArrayList<Set> sets,
                            ViewGroup vgSubtitle,
                            ViewGroup vgSets) {

        // Add subtitle layout which includes "Set #", "Reps" and "Weight"
        setsView = layInflater.inflate(R.layout.exercise_subtitle_item, null);
        vgSubtitle.addView(setsView, 0);

        // Add the listView layout which contains a textView and a recyclerVIew
        setsView = layInflater.inflate(R.layout.exercise_listview_item, null);
        setsView.setId(id);
        vgSets.addView(setsView, 0);

        // Set the title in the textView within the listView layout above
        lblExerciseName = (TextView) setsView.findViewById(R.id.lblExerciseName);
        lblExerciseName.setText(exerciseName);

        // Set the recyclerView list to be horizontal and pass in the exercise sets through the
        // adapter
        setList = (RecyclerView) setsView.findViewById(R.id.rvExerciseSets);
        setList.setHasFixedSize(true);
        setList.setLayoutManager(new LinearLayoutManager(this,
                                 LinearLayoutManager.HORIZONTAL,
                                 false));
        setList.setAdapter(new SetsAdapter(sets));
    }
    // ============================================================================================


    // Workout Timer handling for rest time between exercises
    // ============================================================================================
    /* Animates the snack bar timer sliding up into video if its not visible. Next the timer is set
     * to countdown and a pause label is used to control pausing or resuming the timer.
     * Called in StartWorkoutFragment
     */
    public void resetTimer(long ms) {
        if (timerSnack.getVisibility() == View.GONE) {
            timerSnack.startAnimation(slide_up);
        } else {
            countDownTimer.cancel();
        }

        resetCountDownTimer(ms);
        lblTimerCtrl.setEnabled(true);
    }

    /* Resets the CountDownTimer by creating a new instance with the given milliseconds.
     */
    void resetCountDownTimer(long milliseconds) {
        countDownTimer = getCountDownTimer(milliseconds);
        countDownTimer.start();
        lblTimerCtrl.setText(R.string.pause);
    }

    /* Creates and returns a new CountDownTimer with the rest time to count down from. Has the
     * following format: 0:00
     */
    CountDownTimer getCountDownTimer(final long milliseconds) {

        return new CountDownTimer(milliseconds, 1000) {

            String timerText = "";
            long seconds;
            long minutes;

            // Calculates the minutes and seconds to display in 0:00 format
            public void onTick(long millisUntilFinished) {
                currTime = millisUntilFinished;
                seconds = currTime / 1000;
                minutes = seconds / 60;
                seconds = seconds % 60;

                timerText = getString(R.string.rest_time_remaining) +
                        "\t" +
                        minutes +
                        ":" +
                        String.format(Locale.getDefault(), "%02d", seconds);
                lblTimer.setText(timerText);
            }

            // Changes the timer text, when it gets to 0:00, to "Start the next set"
            public void onFinish() {
                lblTimer.setText(R.string.start_next_set);
                lblTimerCtrl.setEnabled(false);
                lblTimerCtrl.setText("");
            }
        };
    }

    /* Animates the snack bar timer sliding down until its not visible.
     * Called in StartWorkoutFragment.
     *      - Used to hide the timer when the user is doing warmup exercises (rest time not needed)
     */
    public void hideTimer() {
        if (timerSnack.getVisibility() == View.VISIBLE) {
            timerSnack.startAnimation(slide_down);
            countDownTimer.cancel();
            lblTimerCtrl.setEnabled(false);
        }
    }

    /* Functions for implementing AnimationListener
     * For the snackbar timer sliding down, the visibility is set to GONE before starting the
     * animation.
     * For the slide up animation, the visibility is set after it has fully come into place.
     */
    @Override
    public void onAnimationStart(Animation animation) {
        if (animation == slide_down) {
            timerSnack.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if (animation == slide_up) {
            timerSnack.setVisibility(View.VISIBLE);
        }
        animation.cancel();
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
    }
    // ============================================================================================


    // Click handling
    // ============================================================================================
    public void handleClick(int id) {
        switch (id) {

            // Handles onNavigationItemSelected: Navigates through fragments
            // ------------------------------------------------------------------------------------
            case R.id.actWarmups:
                fmt = WARMUPS_FMT;
                break;

            case R.id.actExercises:
                fmt = EXERCISES_FMT;
                break;

            case R.id.actStartWorkout:
                fmt = START_WORKOUT_FMT;
                break;
            // ------------------------------------------------------------------------------------

            // Handles onClick: Handles the timer control in the snackbar timer
            // ------------------------------------------------------------------------------------
            case R.id.lblTimerCtrl:
                String timerCtrl = lblTimerCtrl.getText().toString();

                // If text for label btn is resume, then timer reset using the last displayed time
                if (timerCtrl.equals(getString(R.string.resume))) {
                    resetCountDownTimer(currTime);

                // Timer is paused using cancel
                } else {
                    countDownTimer.cancel();
                    lblTimerCtrl.setText(R.string.resume);
                }

                break;
            // ------------------------------------------------------------------------------------

            // Handles onOptionsItemSelected
            // ------------------------------------------------------------------------------------
            case R.id.actEndWorkout:
                finish();
                break;
            // ------------------------------------------------------------------------------------
        }
    }

    // Navbar functions
    // --------------------------------------------------------------------------------------------
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int itemId = item.getItemId();
        if (itemId != fmtItemId) {
            fmtItemId = itemId;

            fmtTxn = fmtMgr.beginTransaction();

            // Hide last fragment
            fmtTxn.hide(fmt);

            handleClick(itemId);

            // Show the selected fragment in fmt
            fmtTxn.show(fmt);

            fmtTxn.commit();
        }

        return true;
    }
    // --------------------------------------------------------------------------------------------

    // Button and Menu Click Overrides
    // --------------------------------------------------------------------------------------------
    @Override
    public void onClick(View v) {
        handleClick(v.getId());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_start_workout_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        handleClick(item.getItemId());
        return super.onOptionsItemSelected(item);
    }

    // back btn pressed functionality
    // Todo: add saving state of last exercise session
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
    // --------------------------------------------------------------------------------------------
    // ============================================================================================
}
