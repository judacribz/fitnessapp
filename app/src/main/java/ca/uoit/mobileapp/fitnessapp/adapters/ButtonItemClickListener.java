package ca.uoit.mobileapp.fitnessapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**************************************************************************************************
 * adapters -> ButtonItemClickListener.java                                                       *
 **************************************************************************************************
 *
 * Author(s)            | Content Added
 * ------------------------------------------------------------------------------------------------
 * Sheron Balasingam    | Created class to implement RecyclerViews OnItemTouchLister to use with
 *                      | interface in WorkoutsActivity for workouts list click handling
 *-------------------------------------------------------------------------------------------------
 *                      |
 *
 * Resource(s)
 * ------------------------------------------------------------------------------------------------
 * https://stackoverflow.com/questions/24471109/recyclerview-onclick/
 *
 * ================================================================================================
 * Used by WorkoutsActivity.
 *
 * Wrapper onItemTouchListener class for RecyclerView to handle workout list click handling.
 *
 **************************************************************************************************/
public class ButtonItemClickListener implements RecyclerView.OnItemTouchListener {

    // Interfaces
    // ============================================================================================
    // Returns info on item that was clicked
    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
    // ============================================================================================


    private RecyclerView recView;
    private GestureDetector gestureDetector;

    // Constructor
    public ButtonItemClickListener(Context context,
                                   RecyclerView recView,
                                   OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
        this.recView = recView;

        gestureDetector = getGestureDetector(context);
    }


    /* Returns a gesture detector that handles a tap. This is setup to send the view clicked on in
     * the recyclerView through the interface OnItemClickListener
     */
    private GestureDetector getGestureDetector(Context context) {

        return new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onSingleTapUp(MotionEvent event) {

                View childView = recView.findChildViewUnder(event.getX(), event.getY());

                onItemClickListener.onItemClick(childView,
                        recView.getChildAdapterPosition(childView));

                return false;
            }
        });
    }


    /* Override the onInterceptTouchEvent to handle only when the set gestureDetector is triggered
     * by a click (onSingleTapUp)
     */
    @Override
    public boolean onInterceptTouchEvent(RecyclerView recView, MotionEvent event) {
        View childView = recView.findChildViewUnder(event.getX(), event.getY());

        if (childView != null) {
            gestureDetector.onTouchEvent(event);
        }

        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView recView, MotionEvent event) {
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
    }
}