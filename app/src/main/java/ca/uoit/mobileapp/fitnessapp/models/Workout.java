package ca.uoit.mobileapp.fitnessapp.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**************************************************************************************************
 * models -> Workout.java                                                                         *
 * ************************************************************************************************
 *
 * Author(s)            | Content Added
 * ------------------------------------------------------------------------------------------------
 * Sheron Balasingam    | Constructors/getters/setters.
 *                      | Function to create a map object used to write to firebase db.
 *-------------------------------------------------------------------------------------------------
 *                      |
 *
 * Resource(s)
 * ------------------------------------------------------------------------------------------------
 * https://firebase.google.com/docs/database/android/read-and-write
 *
 * ================================================================================================
 * Used to store exercise information.
 *
 * Exercise object with the following attributes:
 *      name:      name of the workout.
 *      exercises: list of Exercise type objects for the workout.
 *
 **************************************************************************************************/
public class Workout {

    private ArrayList<Exercise> exercises;
    private String name;

    // Constructor
    public Workout(String name, ArrayList<Exercise> exercises) {
        this.name = name;
        this.exercises = exercises;
    }


    // Getters and setters
    // ============================================================================================
    public ArrayList<Exercise> getExercises() {
        return exercises;
    }

    public String getName() {
        return this.name;
    }
    // ============================================================================================


    // Helper function used to store Workout information in the firebase db
    public Map<String, Object> toMap() {
        Map<String, Object> workout = new HashMap<>();

        for (Exercise exercise: exercises) {
            workout.put(exercise.getName(), exercise.toMap());
        }

        return workout;
    }
}