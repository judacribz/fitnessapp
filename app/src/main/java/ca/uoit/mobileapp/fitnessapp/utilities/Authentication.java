package ca.uoit.mobileapp.fitnessapp.utilities;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.Patterns;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FacebookAuthCredential;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import ca.uoit.mobileapp.fitnessapp.R;

/**************************************************************************************************
 * utilities -> Authentication.java                                                               *
 * ************************************************************************************************
 *
 * Author(s)            | Content Added
 * ------------------------------------------------------------------------------------------------
 * Sheron Balasingam    | Wrapper functions for the following firebase authentication methods:
 *                      | createUserWithEmailAndPassword, signInWithCredential and signOut.
 *                      | Function for validating a form using email and password.
 *-------------------------------------------------------------------------------------------------
 * Zainab Al Sultan     | Function: deleteAccount(), used to delete a user from firebase
 *                      | Function: reauthenticate(), used to help deleting a user that logged in for a long time
 *
 * Resource(s)
 * ------------------------------------------------------------------------------------------------
 * https://firebase.google.com/docs/auth/android/
 * https://firebase.google.com/docs/auth/android/manage-users
 *
 * ================================================================================================
 * Class of helper functions for authentication.
 *
 **************************************************************************************************/
public class Authentication {

    private static final FirebaseAuth mAuth            = FirebaseAuth.getInstance();
    private static final int          MIN_PASSWORD_LEN = 6;


    // Wrapper function to create an user in Firebase
    public static void createUser(final Activity activity, String email, String password) {

        mAuth.createUserWithEmailAndPassword(email, password)

                // Listener for when a user create attempt finishes
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {

                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        // Sign up success
                        if (task.isSuccessful()) {
                            Toast.makeText(activity, "Sign Up Success!", Toast.LENGTH_SHORT).show();

                            // Sign up fail
                        } else {
                            Toast.makeText(activity,
                                           "Failed to create user",
                                           Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }

    // Wrapper function to log in an user using a firebase credential
    public static void signIn(final Activity activity,
                              AuthCredential credential) {

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        // If sign in successful, handle in AuthStateListener in the activity
                        if (!task.isSuccessful()) {
                            Toast.makeText(activity, "Sign Up Failed", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    // Wrapper function to sign out user
    public static void signOut() {
        FirebaseAuth.getInstance().signOut();
    }

    // Validates login and sign up forms using email and password combination
    public static boolean validateForm(Activity activity, EditText txtEmail, EditText txtPassword) {
        boolean formIsValid = false;
        String email = txtEmail.getText().toString().trim();
        String password = txtPassword.getText().toString().trim();

        if (!email.isEmpty() && !password.isEmpty()) {
            formIsValid = true;
        } else {
            if (email.isEmpty()) {
                txtEmail.setError(activity.getString(R.string.required));
            }

            if (password.isEmpty()) {
                txtPassword.setError(activity.getString(R.string.required));
            }
        }

        if (formIsValid) {
            // Check to see email is in the correct format
            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                txtEmail.setError(activity.getString(R.string.email_format_required));
                formIsValid = false;
            }

            // Check for password at min length
            if (password.length() < MIN_PASSWORD_LEN) {
                txtPassword.setError(activity.getString(R.string.password_minimum_error));
                formIsValid = false;
            }
        }

        return formIsValid;
    }

    public static void reauthenticate(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

    // Get auth credentials from the user for re-authentication. The example below shows
    // email and password credentials but there are multiple possible providers,
    // such as GoogleAuthProvider or FacebookAuthProvider.
        AuthCredential credential = EmailAuthProvider
                .getCredential("user@example.com", "password1234");



   // Prompt the user to re-provide their sign-in credentials
        if (user != null) {
            user.reauthenticate(credential)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Log.i("reauthenticate","Successful");
                        }
                    });
        }
    }


    public static void deleteAccount(){
        reauthenticate();

            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

            if(user != null){
                user.delete()
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Log.i("deleteAccount","User account deleted");

                                }
                            }
                        });

        }


    }
}
