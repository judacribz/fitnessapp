package ca.uoit.mobileapp.fitnessapp;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.*;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import ca.uoit.mobileapp.fitnessapp.models.User;
import ca.uoit.mobileapp.fitnessapp.models.WorkoutHelper;
import com.google.android.gms.common.SignInButton;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.*;

import java.io.IOException;

import static ca.uoit.mobileapp.fitnessapp.utilities.Authentication.*;
import static ca.uoit.mobileapp.fitnessapp.utilities.HelperFunctions.*;
import static ca.uoit.mobileapp.fitnessapp.utilities.Firebase.setFirebaseWorkouts;
import static ca.uoit.mobileapp.fitnessapp.MainActivity.EXTRA_LOG_OUT_USER;

/**************************************************************************************************
 * LoginActivity.java                                                                             *
 * ************************************************************************************************
 *
 * Author(s)                | Content Added
 * ------------------------------------------------------------------------------------------------
 * Sheron Balasingam        | Using firebase authentication with email and password.
 *                          | Handles user login success by adding files from firebase db to local
 *                          | db if the db or user does not exist(first time login).
 *-------------------------------------------------------------------------------------------------
 * Zainab Al Sultan         | login using facebook
 * ------------------------------------------------------------------------------------------------
 * Mirical Williams-Causton | login using Google
 *
 * Resource(s)
 * ------------------------------------------------------------------------------------------------
 * https://firebase.google.com/docs/auth/android/
 * https://firebase.google.com/docs/database/android/read-and-write
 * https://www.lynda.com/Google-Play-Services-tutorials/Use-Google-Sign/
 * https://www.shareicon.net/tag/gymnast?p=4
 *
 * ================================================================================================
 * Logs in the user using email/password, google signIn or facebook signIn. If user logs in for the
 * first time, the workouts under "default_workouts/" in firebase db would be copied under the user
 * and copied to the local db. If the user already exists in firebase, then the workouts under
 * "users/<uid>/workouts/" (uid being the active user's unique firebase identifier) in the firebase
 * db would be copied into the local db.
 *
 **************************************************************************************************/
public class LoginActivity extends AppCompatActivity
        implements FirebaseAuth.AuthStateListener,
                   GoogleApiClient.OnConnectionFailedListener,
                   View.OnClickListener {

    // Constants
    // ============================================================================================
    private final static int GOOGLE_SIGN_IN_REQUEST = 101;
    private static final String TAG = "FacebookLogin";
    private static final String LOGIN_IMAGE = "squat.png";
    // ============================================================================================

    // Global Vars
    // ============================================================================================
    FirebaseAuth mAuth;
    AuthCredential credential;
    CallbackManager mCallbackManager;
    WorkoutHelper workoutHelper;

    ImageView ivMainIcon;
    EditText txtEmail, txtPassword;
    GoogleSignInOptions googleSignInOptions;
    GoogleApiClient googleApiClient;
    SignInButton signInButton;

    Intent signInResult;
    // ============================================================================================


    // LoginActivity @Override
    ///////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setToolBar(LoginActivity.this, R.string.app_name, false);

        // Sign out depending on flag sent by main activity
        signInResult = getIntent();
        if (getIntent().getBooleanExtra(EXTRA_LOG_OUT_USER, false)) {
            signOut();
        }

        ivMainIcon = (ImageView) findViewById(R.id.ivMainIcon);

        // Email/password sign in fields
        txtEmail = findViewById(R.id.txtEmail);
        txtPassword = findViewById(R.id.txtPassword);

        // Get and set widget references
        ((Button) findViewById(R.id.btnLogin)).setOnClickListener(this);
        ((TextView) findViewById(R.id.lblSignUp)).setOnClickListener(this);

        // Google btn setup
        signInButton = (SignInButton) findViewById(R.id.btnGoogleSignIn);
        signInButton.setColorScheme(SignInButton.COLOR_AUTO);
        signInButton.setOnClickListener(this);

        //sign in using facebook login button
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        AppEventsLogger.activateApp(this);

        //Initialize facebook login button
        LoginButton facebookLoginButton = findViewById(R.id.btnFacebookLogin);
        mCallbackManager = CallbackManager.Factory.create();
        facebookLoginButton.setReadPermissions("email","public_profile");
        facebookLoginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook: onSuccess" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
                Toast.makeText(getApplicationContext(),"canceled",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
                Toast.makeText(getApplicationContext(),"Error",Toast.LENGTH_SHORT).show();
            }
        });


        // Set google sign in options and client
        googleSignInOptions = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        googleApiClient =
                new GoogleApiClient.Builder(this).enableAutoManage(
                        this, this
                ).addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions).build();

        // Get firebase authentication instance
        mAuth = FirebaseAuth.getInstance();

        // Setup main image
        AssetManager assetManager = getAssets();

        Bitmap bitmap;
        try {
            bitmap = BitmapFactory.decodeStream(assetManager.open(LOGIN_IMAGE));
            ivMainIcon.setImageBitmap(bitmap);
        } catch (IOException io) {
            io.printStackTrace();
        }
    }

    // Add authStateListener onStart
    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(this);
    }

    // Handle google and facebook login requests
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(mCallbackManager.onActivityResult(requestCode, resultCode, data)) {
            return;
        }

        if (resultCode == RESULT_OK) {

            // SignUp activity return handling
            switch (requestCode) {
                case GOOGLE_SIGN_IN_REQUEST:
                    // Extracting google sign in result
                    GoogleSignInResult googleSignInResult
                            = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                    signInResultHandler(googleSignInResult);
                    break;
            }
        }
    }

    // Remove authStateListener onStop
    @Override
    public void onStop() {
        super.onStop();
        mAuth.removeAuthStateListener(this);
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////


    // Listener to handle all login types through firebase if successful
    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        FirebaseUser fbUser = firebaseAuth.getCurrentUser();

        // User is signed in
        if (fbUser != null) {
            final String email = fbUser.getEmail();
            final String uid = fbUser.getUid();
            Toast.makeText(this, "Logged in as " + email, Toast.LENGTH_SHORT).show();

            // Set email for singleton User
            User user = User.getInstance();
            user.setEmail(email);
            user.setUid(uid);

            // Get local db instance
            workoutHelper = new WorkoutHelper(getApplicationContext());

            /* If the db doesn't exist, or the users email does not exist in the db then get
             * reference to the default workouts from firebase and create the local db using
             * these values
             */
            if (!dbExists(getApplicationContext()) || !workoutHelper.emailExists()) {

                DatabaseReference userRef =
                        FirebaseDatabase.getInstance().getReference("users/" + uid);

                userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        // FIREBASE: if user does not exist copy default workouts under user and
                        // copy same values to local db
                        if (dataSnapshot.getValue() == null) {
                            setFirebaseWorkouts(LoginActivity.this,
                                                getString(R.string.default_workouts));

                            // FIREBASE: if user exists, use users workouts to save in database
                        } else {
                            setFirebaseWorkouts(LoginActivity.this, "users/" + uid + "/workouts");
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            }

            // Set the result and return to MainActivity
            setResult(RESULT_OK, signInResult);
            finish();
        }
    }


    // GOOGLE: sign in handler
    public void signInResultHandler(GoogleSignInResult googleSignInResult) {
        if (googleSignInResult.isSuccess()) {
            GoogleSignInAccount googleSignInAccount = googleSignInResult.getSignInAccount();

            if (googleSignInAccount != null) {
                credential = GoogleAuthProvider.getCredential(googleSignInAccount.getIdToken(),
                        null);
                signIn(this, credential);
            }


        } else {
            Status status = googleSignInResult.getStatus();
            int statusCode = status.getStatusCode();

            if (statusCode == GoogleSignInStatusCodes.SIGN_IN_CANCELLED) {
                Toast.makeText(this, "Sign In Cancelled", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, "Connection failed", Toast.LENGTH_SHORT).show();
    }


    // FACEBOOK
    private void handleFacebookAccessToken(AccessToken accessToken) {
        Log.d(TAG, "handleFacebookAccessToken:" + accessToken);

        credential = FacebookAuthProvider.getCredential(accessToken.getToken());
        signIn(this, credential);
    }


    // Click handling
    //=============================================================================================
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                signInUser();
                break;
            case R.id.lblSignUp:
                startSignup();
                break;
            case R.id.btnGoogleSignIn:
                googleSignIn();
                break;

        }
    }

    // onClick: @id/btnLogin handler
    public void signInUser() {
        if (validateForm(this, txtEmail, txtPassword)) {
            String email = txtEmail.getText().toString().trim();
            String password = txtPassword.getText().toString().trim();

            // Email/Password sign in to firebase
            credential = EmailAuthProvider.getCredential(email, password);
            signIn(this, credential);
        }
    }

    // onClick: @id/lblSignup handler
    public void startSignup() {
        Intent signupActivity = new Intent(this, SignupActivity.class);
        startActivity(signupActivity);
    }

    //onClick: @id/btnGoogleSignIn handler
    public void googleSignIn() {
        Intent googleSignInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(googleSignInIntent, GOOGLE_SIGN_IN_REQUEST);
    }

    // Exits app and goes to home screen if back pressed twice from this screen
    @Override
    public void onBackPressed() {
        handleBackButton(this);
    }
    // ============================================================================================
}