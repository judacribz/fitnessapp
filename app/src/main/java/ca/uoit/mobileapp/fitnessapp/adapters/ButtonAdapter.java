package ca.uoit.mobileapp.fitnessapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import ca.uoit.mobileapp.fitnessapp.R;

/**************************************************************************************************
 * adapters -> ButtonAdapter.java                                                                 *
 * ************************************************************************************************
 *
 * Author(s)            | Content Added
 * ------------------------------------------------------------------------------------------------
 * Sheron Balasingam    | Set up the adapter and the viewHolder for it
 *-------------------------------------------------------------------------------------------------
 *                      |
 * Resource(s)
 * ------------------------------------------------------------------------------------------------
 * https://classroom.udacity.com/courses/ud851/lessons/
 *
 * ================================================================================================
 * Used by WorkoutsActivity.
 *
 * Adapter used for a list of buttons.
 *
 **************************************************************************************************/
public class ButtonAdapter extends RecyclerView.Adapter<ButtonAdapter.ButtonViewHolder> {

    private int numItems;
    private ArrayList<String> btnNames;

    // Adapter Constructor
    public ButtonAdapter(ArrayList<String> btnNames) {
        this.numItems = btnNames.size();
        this.btnNames = btnNames;
    }

    // ButtonAdapter @Override
    ///////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public ButtonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.button_list_item, parent, false);

        return new ButtonViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ButtonViewHolder holder, int position) {
        holder.bind(btnNames.get(position));
    }

    @Override
    public int getItemCount() {
        return numItems;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////


    // Custom ViewHolder class for the recyclerView
    // ============================================================================================
    class ButtonViewHolder extends RecyclerView.ViewHolder {

        private Button listItemButtonView;

        // ViewHolder Constructor
        ButtonViewHolder(View itemView) {
            super(itemView);

            listItemButtonView = (Button) itemView.findViewById(R.id.btnListItem);
        }

        // Sets the text for each button item
        void bind(String btnText) {
            listItemButtonView.setText(btnText);
        }
    }
    // ============================================================================================
}