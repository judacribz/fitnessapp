package ca.uoit.mobileapp.fitnessapp.utilities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.preference.Preference.OnPreferenceClickListener;
import android.text.TextUtils;
import android.widget.Toast;
import android.widget.Toolbar;

import java.util.Objects;

import ca.uoit.mobileapp.fitnessapp.R;

/**************************************************************************************************
 * Preferences.java                                                                            *
 * ************************************************************************************************
 *
 * Author(s)            | Content Added
 * ------------------------------------------------------------------------------------------------
 * Zainab Al Sultan     | change theme -> allows the user to change the app theme
 *                      | delete account -> allows user to delete the account.
 *-------------------------------------------------------------------------------------------------
 *
 *
 **************************************************************************************************/


public class Preferences extends PreferenceActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

//        ListPreference preference = (ListPreference) findPreference("example_list");
//        bindPreferenceSummaryToValue(preference);
//        //CharSequence currText = preference.getEntry();
//        String currValue = preference.getValue();
//
//        if(currValue.equals("0")){
//            Toast toast = Toast.makeText(getApplicationContext(),"theme is blue", Toast.LENGTH_SHORT);
//            toast.show();
//
//            setTheme(R.style.BlueTheme);
//
//        } else{
//
//            Toast toast = Toast.makeText(getApplicationContext(),"theme is black", Toast.LENGTH_SHORT);
//            toast.show();
//
//            setTheme(R.style.BlackTheme);
//
//        }

        Preference account_preference = (Preference) findPreference("delete_account");
        account_preference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
            Intent intent = new Intent(getApplicationContext(), Account.class);
            startActivity(intent);
                return true;
            }
        });

    }

    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            String stringValue = value.toString();

            if (preference instanceof ListPreference) {
                // For list preferences, look up the correct display value in
                // the preference's 'entries' list.
                ListPreference listPreference = (ListPreference) preference;
                int index = listPreference.findIndexOfValue(stringValue);

                // Set the summary to reflect the new value.
                preference.setSummary(
                        index >= 0
                                ? listPreference.getEntries()[index]
                                : null);

            }  else {
                // For all other preferences, set the summary to the value's
                // simple string representation.
                preference.setSummary(stringValue);
            }
            return true;
        }
    };

    private static void bindPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.
        sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.getContext())
                        .getString(preference.getKey(), ""));
    }
}