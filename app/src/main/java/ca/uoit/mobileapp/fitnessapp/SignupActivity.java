package ca.uoit.mobileapp.fitnessapp;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toolbar;

import java.io.IOException;

import static ca.uoit.mobileapp.fitnessapp.utilities.Authentication.*;
import static ca.uoit.mobileapp.fitnessapp.utilities.HelperFunctions.setToolBar;

/**************************************************************************************************
 * SignupActivity.java                                                                            *
 * ************************************************************************************************
 *
 * Author(s)            | Content Added
 * ------------------------------------------------------------------------------------------------
 * Sheron Balasingam    | Sign up user to firebase using email and password.
 *                      | Moved createUser function to Firebase.java.
 *-------------------------------------------------------------------------------------------------
 *                      |
 * Resource(s)
 * ------------------------------------------------------------------------------------------------
 * https://firebase.google.com/docs/auth/android/password-auth
 * https://thenounproject.com/term/overweight/687021/
 *
 * ================================================================================================
 * Creates user in firebase authentication with email and password.
 *
 **************************************************************************************************/
public class SignupActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String SIGNUP_IMAGE = "fatman.png";
    EditText etEmail, etPassword;
    ImageView ivMainIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        setToolBar(SignupActivity.this, getString(R.string.create_account), false);

        ivMainIcon = (ImageView) findViewById(R.id.ivMainIcon);
        etEmail = (EditText) findViewById(R.id.txtEmail);
        etPassword = (EditText) findViewById(R.id.txtPassword);

        ((Button) findViewById(R.id.btnSignup)).setOnClickListener(this);
        ((TextView) findViewById(R.id.lblLogin)).setOnClickListener(this);

        // Setup main image
        AssetManager assetManager = getAssets();

        Bitmap bitmap;
        try {
            bitmap = BitmapFactory.decodeStream(assetManager.open(SIGNUP_IMAGE));
            ivMainIcon.setImageBitmap(bitmap);
        } catch (IOException io) {
            io.printStackTrace();
        }
    }


    // Click handling
    //=============================================================================================
    private void handleClick(int id) {
        switch (id) {
            case R.id.btnSignup:
                signUp();
                break;

            case R.id.lblLogin:
                finish();
                break;
        }
    }

    // onClick: @id/btnSignup handler
    public void signUp() {
        String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();

        // Makes sure the forms aren't empty when the singup button is pressed
        if (validateForm(this, etEmail, etPassword)) {
            createUser(this, email, password);
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        handleClick(v.getId());
    }
    //=============================================================================================
}
