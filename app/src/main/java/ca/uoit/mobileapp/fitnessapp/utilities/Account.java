package ca.uoit.mobileapp.fitnessapp.utilities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import static ca.uoit.mobileapp.fitnessapp.utilities.Authentication.*;

import com.google.firebase.auth.FirebaseAuth;

import ca.uoit.mobileapp.fitnessapp.MainActivity;
import ca.uoit.mobileapp.fitnessapp.R;
import ca.uoit.mobileapp.fitnessapp.models.User;

import static ca.uoit.mobileapp.fitnessapp.utilities.HelperFunctions.setToolBar;

/**************************************************************************************************
 * utilities -> Account.java                                                               *
 * ************************************************************************************************
 *
 * Author(s)            | Content Added
 * ------------------------------------------------------------------------------------------------

 * Zainab Al Sultan     | User can delete his/her account
 *                      |
 **************************************************************************************************/

public class Account extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        setToolBar(this,"Delete Account", true);

        Button cancel = (Button) findViewById(R.id.btn_cancel);
        cancel.setOnClickListener(this);

        TextView tv_email = findViewById(R.id.lbl_userEmail);
       String email = User.getInstance().getEmail();

       tv_email.setText(email);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    public void delete(View view) {
        signOut();
        deleteAccount();

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_cancel:
                finish();
                break;
        }
    }
}
