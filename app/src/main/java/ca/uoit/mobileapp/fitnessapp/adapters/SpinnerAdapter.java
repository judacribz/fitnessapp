package ca.uoit.mobileapp.fitnessapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import ca.uoit.mobileapp.fitnessapp.R;

/**************************************************************************************************
 * adapters -> SpinnerAdapter.java                                                                   *
 * ************************************************************************************************
 *
 * Author(s)                 | Content Added
 * ------------------------------------------------------------------------------------------------
 * Mirical Williams-Causton  | Created class to populate spinners
 *
 * ================================================================================================
 * Used to populate the spinners
 **************************************************************************************************/

public class SpinnerAdapter extends ArrayAdapter<String>{
    private List<String> exerciseList;
    private Context context;

    public SpinnerAdapter(Context context, List<String> exerciseList) {
        super(context, R.layout.support_simple_spinner_dropdown_item, exerciseList);
        this.context = context;
        this.exerciseList = exerciseList;
    }

    @Override
    public View getDropDownView(int position, View reusableView, @NonNull ViewGroup parent) {
        String exercise = this.exerciseList.get(position);

        if (reusableView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            reusableView = inflater.inflate(R.layout.support_simple_spinner_dropdown_item, parent, false);
        }

        TextView lblSpinner = (TextView) reusableView.findViewById(android.R.id.text1);
        lblSpinner.setText(exercise);

        return reusableView;
    }
}
