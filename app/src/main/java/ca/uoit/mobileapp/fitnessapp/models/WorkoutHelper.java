package ca.uoit.mobileapp.fitnessapp.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**************************************************************************************************
 * models -> WorkoutHelper.java                                                                   *
 * ************************************************************************************************
 *
 * Author(s)                 | Content Added
 * ------------------------------------------------------------------------------------------------
 * Sheron Balasingam         | Creates workouts table.
 *                           | Adds workouts/exercises to the db.
 *                           | Retrieves a workout when given the name of the workout.
 *                           | Function to retrieve all workouts and another to retrieve workout names.
 *                           | Function to delete all workouts and another to delete a specified
 *                           | workout.
 *-------------------------------------------------------------------------------------------------
 * Mirical Williams-Causton  | Created two functions, one to retrieve exercise names and another
 *                           | to retrieve exercises
 *
 * ================================================================================================
 * Used to store and access workout information for users.
 *
 * workouts table:
 *      workoutName:  name of the workout the exercise belongs to.
 *      exerciseName: name of the exercise.
 *      exerciseType: type of exercise such as strength or cardio.
 *      equipment:    type of equipment needed for the exercise such as a barbell or dumbbells.
 *      reps:         number of repetitions for the exercise.
 *      sets:         number of times the repetitions of the exercise will be done.
 *      weight:       amount of weight that will be lifted (if type=strength)
 *      email:        email of user that the workout was created under
 *
 **************************************************************************************************/
public class WorkoutHelper extends SQLiteOpenHelper {

    // Constants
    // ============================================================================================
    private static final int    DATABASE_VERSION = 1;
    private static final String TABLE            = "workouts";

    // Column names
    private static final String WORKOUT_NAME  = "workoutName";
    private static final String EXERCISE_NAME = "exerciseName";
    private static final String EXERCISE_TYPE = "exerciseType";
    private static final String EQUIPMENT     = "equipment";
    private static final String SETS          = "sets";
    private static final String REPS          = "reps";
    private static final String WEIGHT        = "weight";
    private static final String EMAIL         = "email";

    // Workouts table create statement
    private static final String CREATE_STATEMENT =
            "CREATE TABLE " + TABLE + " (" +
                    WORKOUT_NAME  + " text not null," +
                    EXERCISE_NAME + " text not null," +
                    EXERCISE_TYPE + " text not null," +
                    EQUIPMENT     + " text," +
                    SETS          + " integer not null," +
                    REPS          + " integer not null," +
                    WEIGHT        + " real," +
                    EMAIL         + " varchar2(100) not null," +
                    "PRIMARY KEY (" + WORKOUT_NAME + ", " + EXERCISE_NAME + ", " + EMAIL + ")" +
            ")\n";

    private static final String DROP_STATEMENT = "DROP TABLE workouts";
    // ============================================================================================

    private String email;
    private Context context;

    // Constructor
    public WorkoutHelper(Context context) {
        super(context, "workouts", null, DATABASE_VERSION);
        this.context = context;
        this.email = User.getInstance().getEmail();
    }

    // WorkoutHelper @Override
    ///////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void onCreate(SQLiteDatabase db) {

        // create the database, using CREATE SQL statement
        db.execSQL(CREATE_STATEMENT);
    }

    // TODO: needs to save data from old db before drop and write to new db
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersionNum, int newVersionNum) {

        // delete the old database
        db.execSQL(DROP_STATEMENT);

        // re-create the database
        db.execSQL(CREATE_STATEMENT);
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////


    /* Checks if user's email exists in db
     */
    public boolean emailExists() {
        SQLiteDatabase db = this.getReadableDatabase();
        boolean emailExists = false;

        // Get unique workout names
        String[] column      = new String[] {EMAIL};
        String where         = EMAIL + " = ?";
        String[] whereArgs   = new String[] {email};
        Cursor workoutCursor = db.query(true, // true for unique
                TABLE,
                column,
                where,
                whereArgs,
                EMAIL,
                null,
                null,
                null);

        if (workoutCursor.getCount() == 1) {
            emailExists = true;
        }

        workoutCursor.close();
        return emailExists;
    }


    // CRUD functions
    // ============================================================================================
    // CREATE
    // --------------------------------------------------------------------------------------------
    /* Creates a db entry for each exercise in the workout
     */
    public void addWorkout(Workout workout) {
        SQLiteDatabase db = this.getWritableDatabase();

        ArrayList<Exercise> exercises = workout.getExercises();

        // put that data into the database
        ContentValues newValues = new ContentValues();

        String workoutName = workout.getName();
        for (Exercise exercise : exercises) {
            newValues.put(WORKOUT_NAME,  workoutName);
            newValues.put(EXERCISE_NAME, exercise.getName());
            newValues.put(EXERCISE_TYPE, exercise.getType());
            newValues.put(EQUIPMENT,     exercise.getEquipment());
            newValues.put(SETS,          exercise.getSets());
            newValues.put(REPS,          exercise.getReps());
            newValues.put(WEIGHT,        exercise.getWeight());
            newValues.put(EMAIL,         email);

            db.insert(TABLE, null, newValues);
        }
    }
    // --------------------------------------------------------------------------------------------

    // RETRIEVE
    // --------------------------------------------------------------------------------------------
    /* Gets all exercises for a workout
     */
    public Workout getWorkout(String workoutName) {
        SQLiteDatabase db = this.getReadableDatabase();

        Workout workout = null;
        ArrayList<Exercise> exercises = new ArrayList<>();

        String[] columns = new String[] {EXERCISE_NAME,
                                         EXERCISE_TYPE,
                                         EQUIPMENT,
                                         SETS,
                                         REPS,
                                         WEIGHT};
        String where       = WORKOUT_NAME + " = ? AND " + EMAIL + " = ?";
        String[] whereArgs = new String[] {workoutName, email};
        Cursor cursor = db.query(TABLE, columns, where, whereArgs, "", "", "");

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            do {
                exercises.add(new Exercise(cursor.getString(0),
                                           cursor.getString(1),
                                           cursor.getString(2),
                                           cursor.getInt(3),
                                           cursor.getInt(4),
                                           cursor.getFloat(5)));
            } while (cursor.moveToNext());

            workout = new Workout(workoutName, exercises);
        }

        cursor.close();

        return workout;
    }

    /* Gets all workouts in the db and returns a list of Workout objects
     */
    public ArrayList<Workout> getAllWorkouts() {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<Workout> workouts = new ArrayList<>();

        // Get unique workout names
        String[] column      = new String[] {WORKOUT_NAME};
        String where         = EMAIL + " = ?";
        String[] whereArgs   = new String[] {email};
        Cursor workoutCursor = db.query(true, // true for unique
                TABLE,
                                        column,
                                        where,
                                        whereArgs,
                                        WORKOUT_NAME,
                                        null,
                                        null,
                                        null);

        if (workoutCursor.getCount() > 0) {
            workoutCursor.moveToFirst();
            Workout workout;
            do {
                String workoutName = workoutCursor.getString(0);
                workout = getWorkout(workoutName);
                workouts.add(workout);

            } while (workoutCursor.moveToNext());
        }

        workoutCursor.close();
        return workouts;
    }

    /* Gets all workout names in the db and returns a list of Strings
     */
    public ArrayList<String> getAllWorkoutNames() {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<String> workoutNames = new ArrayList<>();

        // Get unique workout names
        String[] column      = new String[] {WORKOUT_NAME};
        String where         = EMAIL + " = ?";
        String[] whereArgs   = new String[] {email};
        Cursor workoutCursor = db.query(true, // true for unique
                                        TABLE,
                                        column,
                                        where,
                                        whereArgs,
                                        WORKOUT_NAME,
                                        null,
                                        null,
                                        null);

        if (workoutCursor.moveToFirst()) {
            do {
                workoutNames.add(workoutCursor.getString(0));

            } while (workoutCursor.moveToNext());
        }

        workoutCursor.close();
        return workoutNames;
    }

    /* Gets all the unique exercises in the db and returns a list of Exercise objects
     */
    public ArrayList<Exercise> getAllExercises() {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<Exercise> exercises = new ArrayList<>();

        String[] columns = new String[] {EXERCISE_NAME, EXERCISE_TYPE, EQUIPMENT, SETS, REPS, WEIGHT};
        String where         = EMAIL + " = ?";
        String[] whereArgs   = new String[] {email};

        Cursor cursor = db.query(true, TABLE, columns, where, whereArgs, EXERCISE_NAME,"", "", "");

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            do {
                exercises.add(new Exercise(cursor.getString(0), cursor.getString(1), cursor.getString(2),
                        cursor.getInt(3), cursor.getInt(4), cursor.getFloat(5)));

            } while (cursor.moveToNext());
        }
        cursor.close();
        return exercises;
    }

    /* Gets all the unique exercise names in the db and returns a list of Strings
     */
    public ArrayList<String> getAllExerciseNames() {
        SQLiteDatabase db = this.getReadableDatabase();

        ArrayList<String> exerciseNames = new ArrayList<>();

        // Get unique exercise names
        String[] column      = new String[] {EXERCISE_NAME};
        String where         = EMAIL + " = ?";
        String[] whereArgs   = new String[] {email};
        Cursor workoutCursor = db.query(true, // true for unique
                                        TABLE,
                                        column,
                                        where,
                                        whereArgs,
                                        EXERCISE_NAME,
                                        null,
                                        null,
                                        null);

        if (workoutCursor.moveToFirst()) {
            do {
                exerciseNames.add(workoutCursor.getString(0));

            } while (workoutCursor.moveToNext());
        }

        workoutCursor.close();
        return exerciseNames;
    }
    // --------------------------------------------------------------------------------------------

    // UPDATE
    // --------------------------------------------------------------------------------------------
    /* Updates the weight for an exercise in a workout
     */
    public boolean updateWeight(String workoutName, String exerciseName, float newWeight) {
        SQLiteDatabase db = this.getWritableDatabase();


        ContentValues newValues = new ContentValues();
        newValues.put(WEIGHT, newWeight);

        String where = EMAIL + " = ? AND " + WORKOUT_NAME + " = ? AND " + EXERCISE_NAME + " = ?";
        String[] whereArgs = new String[] {email, workoutName, exerciseName};

        int numRows = db.update(TABLE, newValues, where, whereArgs);

        return (numRows == 1);
    }

    // DELETE
    // --------------------------------------------------------------------------------------------
    /* Deletes a workout
     */
    public boolean deleteWorkout(String workoutName) {
        SQLiteDatabase db = this.getWritableDatabase();

        String where       = WORKOUT_NAME + " = ? AND " + EMAIL + " = ?";
        String[] whereArgs = new String[] {workoutName, email};
        int numRows        = db.delete(TABLE, where, whereArgs);

        return (numRows > 0);
    }

    /* Deletes all workouts
     */
    public void deleteAllWorkouts() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE, "", new String[] { });
    }
    // --------------------------------------------------------------------------------------------
    // ============================================================================================
}
