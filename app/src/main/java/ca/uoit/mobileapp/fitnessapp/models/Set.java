package ca.uoit.mobileapp.fitnessapp.models;

/**************************************************************************************************
 * models -> Set.java                                                                             *
 * ************************************************************************************************
 *
 * Author(s)            | Content Added
 * ------------------------------------------------------------------------------------------------
 * Sheron Balasingam    | Constructors/getters/setters.
 *-------------------------------------------------------------------------------------------------
 *                      |
 *
 * ================================================================================================
 * Used to store workout set information.
 *
 * Set object with the following attributes:
 *      setNumber: number used to identify set in a queue of Sets
 *      reps:      number of repetitions for the exercise.
 *      weight:    amount of weight that will be lifted (if type=strength)
 *
 **************************************************************************************************/
public class Set {

    private int setNumber, reps;
    private float weight;

    // Constructor
    public Set(int setNumber, int reps, float weight) {
        this.setNumber = setNumber;
        this.reps = reps;
        this.weight = weight;
    }

    // Getters and setters
    // ============================================================================================
    public int getSets() {
        return setNumber;
    }

    public int getReps() {
        return reps;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }
    // ============================================================================================
}
