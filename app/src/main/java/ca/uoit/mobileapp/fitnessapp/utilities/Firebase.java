package ca.uoit.mobileapp.fitnessapp.utilities;

import android.app.Activity;
import java.util.ArrayList;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import ca.uoit.mobileapp.fitnessapp.R;
import ca.uoit.mobileapp.fitnessapp.models.Exercise;
import ca.uoit.mobileapp.fitnessapp.models.Workout;
import ca.uoit.mobileapp.fitnessapp.models.WorkoutHelper;

/**************************************************************************************************
 *  utilities -> Firebase.java                                                                    *
 * ************************************************************************************************
 *
 * Author(s)            | Content Added
 * ------------------------------------------------------------------------------------------------
 * Sheron Balasingam    | Function (addDefaultsToFirebase) to copy the data in firebase db from
 *                      | "default_workouts/" to "users/<uid>/workouts/" (where <uid> is the
 *                      | current users firebase auth identifier).
 *                      | Function (setFirebaseWorkouts) which calls addDefaultsToFirebase, if the
 *                      | user doesn't exist in firebase and copies that same data to the local db.
 *                      | If the user does exists in firebase, setFirebaseWorkouts is used to copy
 *                      | the data under "users/<uid>/workouts/" to local db.
 *-------------------------------------------------------------------------------------------------
 *                      |
 *
 * Resource(s)
 * -----------------------------------------------------------------------------------------------
 * https://firebase.google.com/docs/database/android/read-and-write
 *
 * ================================================================================================
 * Class of helper functions that interact with the firebase db.
 *
 **************************************************************************************************/
public class Firebase {

    /* Adds data in firebase under "default_workouts/" to "users/<uid>/workouts/"
     */
    private static void addDefaultsToFirebase(final Activity activity) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        final WorkoutHelper workoutHelper = new WorkoutHelper(activity);

        if (user != null) {
            final String email = user.getEmail();
            final String uid = user.getUid();
            final DatabaseReference userRef =
                    FirebaseDatabase.getInstance().getReference("users/" + uid);

            userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    // FIREBASE: add user data
                    userRef.child("email").setValue(email);
                    userRef.child("username");

                    // FIREBASE: add workouts under user/uid/workouts/
                    for (Workout workout : workoutHelper.getAllWorkouts()) {
                        userRef.child("workouts").child(
                                workout.getName()
                        ).setValue(workout.toMap());
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        }
    }


    /* Adds data under "default_workouts/" in firebase to local db. Used with reference String
     * equal to "default_workouts" to call addDefaultsToFirebase function
     */
    public static void setFirebaseWorkouts(final Activity activity, final String reference) {

        // FIREBASE: Get reference to default_workouts
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(reference);

        // Listener for default_workouts object in firebase
        myRef.addValueEventListener(new ValueEventListener() {

            // Saves data to file
            @Override
            public void onDataChange(DataSnapshot workoutsShot) {
                if (workoutsShot != null) {

                    final WorkoutHelper workoutHelper = new WorkoutHelper(activity);
                    ArrayList<Exercise> exercises;
                    ArrayList<Workout> workouts = new ArrayList<>();

                    // get each workout in default_workouts in firebase
                    for (DataSnapshot workoutShot : workoutsShot.getChildren()) {
                        String workoutName = workoutShot.getKey();
                        exercises = new ArrayList<>();
                        Exercise exercise;

                        // Get exercises from each workout in firebase
                        for (DataSnapshot exerciseShot : workoutShot.getChildren()) {
                            exercise = exerciseShot.getValue(Exercise.class);

                            if (exercise != null) {
                                exercise.setName(exerciseShot.getKey());
                                exercises.add(exercise);
                            }
                        }

                        workouts.add(new Workout(workoutName, exercises));
                    }

                    // FIREBASE: get workouts under default_workouts and add it to local database
                    for (Workout workout : workouts) {
                        workoutHelper.addWorkout(workout);
                    }


                    // FIREBASE: Add default workouts and user data
                    if (reference.equals(activity.getString(R.string.default_workouts))) {
                        addDefaultsToFirebase(activity);
                    }

                    workoutHelper.close();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }
}
