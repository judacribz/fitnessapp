package ca.uoit.mobileapp.fitnessapp.utilities;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;
import static ca.uoit.mobileapp.fitnessapp.utilities.HelperFunctions.setToolBar;
import static java.lang.String.format;

import ca.uoit.mobileapp.fitnessapp.MainActivity;
import ca.uoit.mobileapp.fitnessapp.R;

/**************************************************************************************************
 * StepCounterFragment.java                                                                              *
 * ************************************************************************************************
 *
 * Author(s)            | Content Added
 * ------------------------------------------------------------------------------------------------
 * Zainab Al Sultan     | Using sensor manager to count steps
 *
 *-------------------------------------------------------------------------------------------------
 *
 *Resource(s)
 * ------------------------------------------------------------------------------------------------
 * https://www.youtube.com/watch?v=CNGMWnmldaU
 * https://github.com/coomar2841/android-step-sensors
 * ================================================================================================
 *
 *
 **************************************************************************************************/
public class StepCounterFragment extends AppCompatActivity implements SensorEventListener {

    SensorManager sensorManager;
    boolean running = false;

    TextView steps;
    TextView time;

    long last_timeStamp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_counter_fragment);
        setToolBar(this, R.string.step_counter, true);

        steps = findViewById(R.id.lbl_counter);
        time = findViewById(R.id.lbl_time);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume(){


        super.onResume();
        running = true;
        Sensor counter = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        if(counter != null){
            sensorManager.registerListener(this, counter, SensorManager.SENSOR_DELAY_UI);
        } else {
            Toast.makeText(this,"error!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        running = false;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        if(running){
            float value = sensorEvent.values[0];
            steps.setText(String.valueOf(value));
            time.setText("0");

        } else{
            long timeStamp = sensorEvent.timestamp/1000000000;
            long minutes = timeStamp / 60;
            time.setText(String.valueOf(minutes));
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {


    }
}
