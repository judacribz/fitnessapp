package ca.uoit.mobileapp.fitnessapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


import ca.uoit.mobileapp.fitnessapp.utilities.Preferences;



import static ca.uoit.mobileapp.fitnessapp.utilities.HelperFunctions.*;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import ca.uoit.mobileapp.fitnessapp.utilities.StepCounterFragment;

import static ca.uoit.mobileapp.fitnessapp.utilities.Authentication.signIn;
import static ca.uoit.mobileapp.fitnessapp.utilities.Authentication.validateForm;

import static ca.uoit.mobileapp.fitnessapp.utilities.HelperFunctions.handleBackButton;
import static ca.uoit.mobileapp.fitnessapp.utilities.HelperFunctions.setToolBar;

/**************************************************************************************************
 * MainActivity.java                                                                              *
 * ************************************************************************************************
 *
 * Author(s)            | Content Added
 * ------------------------------------------------------------------------------------------------
 * Sheron Balasingam    | Starts LoginActivity in onCreate to check if a user is logged in.
 *                      | Logout btn calls LoginActivity with a true flag to log the user out.
 *-------------------------------------------------------------------------------------------------
 *                      |
 *
 * ================================================================================================
 * The main activity of the app.
 *
 * When the activity is created, LoginActivity is called with a false flag to check if any users
 * are logged in. The false flag is used to tell LoginActivity not to sign out the user. Logout is
 * handled by setting the flag to true.
 *
 **************************************************************************************************/
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    final static String EXTRA_LOG_OUT_USER = "ca.uoit.mobileapp.fitnessapp.EXTRA_LOG_OUT_USER";


    // MainActivity @Override
    ///////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setToolBar(MainActivity.this, R.string.app_name, false);

        ((Button) findViewById(R.id.btnWorkouts)).setOnClickListener(this);
        ((Button) findViewById(R.id.btnStepCounter)).setOnClickListener(this);

        startLogin(false);
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////


    // Click handling
    //=============================================================================================
    public void handleClick(int id) {
        switch (id) {

            // Handles onClick
            case R.id.btnWorkouts:
                startWorkouts();
                break;

            case R.id.actLogout:
                startLogin(true);
                break;

            case R.id.btnStepCounter:
                countSteps();
                break;

            // Handles onOptionsItemSelected
            // TODO: add start settings activity handling
            case R.id.actSettings:
                openSettings();
                break;
        }
    }

    private void openSettings() {
        Intent settingIntent = new Intent(this, Preferences.class);
        startActivity(settingIntent);

    }

    // onClick: @id/btnWorkouts handler
    public void startWorkouts() {
        Intent workoutsActivity = new Intent(this, WorkoutsActivity.class);
        startActivity(workoutsActivity);
    }

    // onClick: @id/btnLogout handler
    public void startLogin(boolean logOutUser) {
        Intent loginActivity = new Intent(this, LoginActivity.class);

        // Tells LoginActivity to sign out current user depending on boolean
        // value of logOutUser
        loginActivity.putExtra(EXTRA_LOG_OUT_USER, logOutUser);
        startActivity(loginActivity);
    }

    // onClick: @id/btnStepCounter handler
    public void countSteps() {
        Intent stepCounterIntent = new Intent(this, StepCounterFragment.class);
        startActivity(stepCounterIntent);
    }

    @Override
    public void onClick(View v) {
        handleClick(v.getId());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        handleClick(item.getItemId());
        return super.onOptionsItemSelected(item);
    }

    // Exits app and goes to home screen if back pressed twice from this screen
    @Override
    public void onBackPressed() {
        handleBackButton(getApplicationContext());
    }
    // ============================================================================================
}
