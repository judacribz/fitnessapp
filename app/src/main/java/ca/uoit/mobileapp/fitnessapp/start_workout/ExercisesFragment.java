package ca.uoit.mobileapp.fitnessapp.start_workout;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

import ca.uoit.mobileapp.fitnessapp.R;
import ca.uoit.mobileapp.fitnessapp.models.Exercise;
import ca.uoit.mobileapp.fitnessapp.models.Set;

/**************************************************************************************************
 * start_workout -> ExercisesFragment.java                                                        *
 * ************************************************************************************************
 *
 * Author(s)            | Content Added
 * ------------------------------------------------------------------------------------------------
 * Sheron Balasingam    | Receives the exercises from StartWorkoutFragment and converts them to an
 *                      | ArrayList of exercise sets to be displayed in horizontal listView format.
 *-------------------------------------------------------------------------------------------------
 *                      |
 *
 * ================================================================================================
 * This fragment is displayed part of StartWorkoutActivity.
 *
 * Displays all the sets of main exercise sets for the current workout.
 *
 **************************************************************************************************/
public class ExercisesFragment extends Fragment
        implements StartWorkoutFragment.OnExercisesReceivedListener {

    StartWorkoutActivity act;
    ViewGroup vgSets, vgSubtitle;

    // Required Empty Constructor
    public ExercisesFragment() {
    }

    // ExercisesFragment @Override
    ///////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        act = (StartWorkoutActivity) getActivity();

        act.START_WORKOUT_FMT.setOnExercisesReceivedListener(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_exercises, container, false);

        vgSubtitle = (ViewGroup) view.findViewById(R.id.llExerciseSubtitleInsert);
        vgSets = (ViewGroup) view.findViewById(R.id.llExerciseSetsInsert);

        return view;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////


    // Interface implementations
    // ============================================================================================
    // Receives the exercise sets from StartWorkoutFragment and sets them up to be displayed in
    // recyclerView lists
    @Override
    public void exercisesReceived(ArrayList<Exercise> exercises) {

        ArrayList<Set> mainSets;
        Exercise exercise;

        for (int i = 0; i < exercises.size(); i++) {
            exercise = exercises.get(i);

            // Create a new list of sets
            mainSets = new ArrayList<>();
            for (int j = 0; j < exercise.getSets(); j++) {
                mainSets.add(new Set(j+1, exercise.getReps(), exercise.getWeight()));
            }

            // Display the sets for the exercise in a horizontal recyclerView
            act.displaySets(i + 1 + 100, exercise.getName(), mainSets, vgSubtitle, vgSets);
        }
    }
    // ============================================================================================
}