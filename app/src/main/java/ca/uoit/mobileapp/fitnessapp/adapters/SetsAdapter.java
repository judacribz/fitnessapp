package ca.uoit.mobileapp.fitnessapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;

import ca.uoit.mobileapp.fitnessapp.R;
import ca.uoit.mobileapp.fitnessapp.models.Set;

/**************************************************************************************************
 * adapters -> SetsAdapter.java                                                                   *
 * ************************************************************************************************
 *
 * Author(s)            | Content Added
 * ------------------------------------------------------------------------------------------------
 * Sheron Balasingam    | Set up the adapter and the viewHolder for it
 *-------------------------------------------------------------------------------------------------
 *                      |
 *
 * ================================================================================================
 * Used by WarmupsFragment and ExercisesFragment.
 *
 * Adapter used for a list of exercise set items (set#, reps, weight).
 *
 **************************************************************************************************/
public class SetsAdapter extends RecyclerView.Adapter<SetsAdapter.SetsViewHolder> {

    private int numberOfSets;
    private ArrayList<Set> sets;

    // Adapter Constructor
    public SetsAdapter(ArrayList<Set> sets) {
        this.sets = sets;
        this.numberOfSets = sets.size();
    }

    // SetsAdapter @Override
    ///////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public SetsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View view = inflater.inflate(R.layout.exercise_list_item, parent, false);
        return new SetsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SetsViewHolder holder, int position) {

        Set set = sets.get(position);
        holder.bind(set.getSets(), set.getReps(), set.getWeight());
    }

    @Override
    public int getItemCount() {
        return numberOfSets;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////


    // Returns the list of sets
    public ArrayList<Set> getArrayList() {
        return sets;
    }


    // Custom ViewHolder class for the recyclerView
    // ============================================================================================
    class SetsViewHolder extends RecyclerView.ViewHolder {

        private TextView tvSets, tvReps, tvWeight;

        // ViewHolder Constructor
        SetsViewHolder(View itemView) {
            super(itemView);

            tvSets = itemView.findViewById(R.id.lblSets);
            tvReps = itemView.findViewById(R.id.txtReps);
            tvWeight = itemView.findViewById(R.id.txtWeight);
        }

        // Sets the text for each list item
        void bind(int setNumber, int reps, float weight) {
            tvSets.setText(String.valueOf(setNumber));
            tvReps.setText(String.valueOf(reps));
            tvWeight.setText(String.valueOf(weight));
        }
    }
}
