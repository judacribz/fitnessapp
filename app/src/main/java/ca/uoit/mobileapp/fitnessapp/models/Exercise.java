package ca.uoit.mobileapp.fitnessapp.models;

import java.util.HashMap;
import java.util.Map;

/**************************************************************************************************
 * models -> Exercise.java                                                                        *
 * ************************************************************************************************
 *
 * Author(s)            | Content Added
 * ------------------------------------------------------------------------------------------------
 * Sheron Balasingam    | Constructors/getters/setters.
 *                      | Function to create a map object used to write to firebase db.
 *-------------------------------------------------------------------------------------------------
 *                      |
 *
 * Resource(s)
 * ------------------------------------------------------------------------------------------------
 * https://firebase.google.com/docs/database/android/read-and-write/
 *
 * ================================================================================================
 * Used to store exercise information.
 *
 * Exercise object with the following attributes:
 *      name:      name of the exercise.
 *      type:      type of exercise such as strength or cardio.
 *      equipment: type of equipment needed for the exercise such as a barbell or dumbbells.
 *      reps:      number of repetitions for the exercise.
 *      sets:      number of times the repetitions of the exercise will be done.
 *      weight:    amount of weight that will be lifted (if type=strength)
 *
 **************************************************************************************************/
public class Exercise {

    private String name, type, equipment;
    private int reps, sets;
    private float weight;

    // Constructors
    // ============================================================================================
    // Required empty constructor for firebase
    public Exercise() {
    }

    public Exercise(String name, String type, String equipment, int sets, int reps, float weight) {
        this.name      = name;
        this.type      = type;
        this.equipment = equipment;
        this.reps      = reps;
        this.sets      = sets;
        this.weight    = weight;
    }
    // ============================================================================================


    // Getters and setters
    // ============================================================================================
    public String getName() {
        return name;
    }

    public int getReps() {
        return reps;
    }

    public int getSets() {
        return sets;
    }

    public float getWeight() {
        return weight;
    }

    public String getType() {
        return type;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setName(String name) {
        this.name = name;
    }
    // ============================================================================================


    // Helper function used to store Exercise information in the firebase db
    Map<String, Object> toMap() {
        Map<String, Object> exercise = new HashMap<>();

        exercise.put("type",      type);
        exercise.put("equipment", equipment);
        exercise.put("sets",      sets);
        exercise.put("reps",      reps);
        exercise.put("weight",    weight);

        return exercise;
    }
}
