# FitnessApp
Final Project for CSCI 4100U (Mobile Devices)

## Developers:
[Zainab](https://github.com/Zainab-Alsultan/), [Sheron](https://github.com/judacribz/), [Mirical](https://github.com/xXGirl-Computer-ScientistXx)

## Authentication:
Google sign-in takes priority so you can't login using facebook or regular sign in if the email is the same.
Facebook login also does not work if the user's facebook account is not added to the fb dev page. When the app is deployed to the playstore, all user's will be able to login using Facebook.


## Code:
[Activities](app/src/main/java/ca/uoit/mobileapp/fitnessapp/)<br />
[Layouts](app/src/main/res/layout/)

## [Todo List](https://docs.google.com/document/d/1OAxaBjVqqWQ7KtLN31HpcIOQZ5OFs7Fo9XksHjPF74g/)
## [Meeting Notes](https://docs.google.com/document/d/17KeeAu4w4C5iN5oSZTcLcJui8MMb8nyavKq3tw9FJz8/)

## [Design](design/):
[Mockup](design/Project.pdf)
